# Carbon calculator - Readme file



## Dépendances utilisées

| Dépendance | Version |
| ----------- | ----------- |
| junit | 4.11 |
| junit-jupiter-engine | 5.7.2 |
| hamcrest-all | 1.3 |
| commons-validator | 1.4.1 |
| javaee-web-api | 8.0.1 |
| hibernate-core | 5.4.0.Final |
| javax.persistence-api | 2.2 |
| commons-codec | 5.7.2 |
| commons-io | 2.6 |
| maven-dependency-plugin | 3.2.0 |
| eclipselink | 3.0.0-RC1 |
| hibernate-entitymanager | 5.2.2.Final |
| postgresql | 42.2.8 |
| jstl | 1.2 |
| jackson-databind | 2.13.0 |



## Comment compiler et générer le fichier .war du site web

Pour générer un fichier .war du site web, suivre les instructions suivantes :
```shell
cd website
mvn clean package
```

:warning: Vous devez être connecté au réseau de la DOUA de Lyon 1. Sinon la commande vous indiquera un message d'erreur!

Si vous souhaitez ne pas exécuter les tests, lancer les instructions suivantes :
```shell
cd website
mvn clean package -Dmaven.test.skip=true
```

Vous devriez obtenir un fichier .war à l'adresse suivante : `racine_du_depot/target/website.war`.



## VM de démo

https://192.168.74.203/website contient la démo du site web.
