-- Création et selection d'une base de donnée vierge
-- DROP DATABASE IF EXISTS footprintcalculator;
-- CREATE DATABASE footprintcalculator;
-- connect TO footprintcalculator;

--	Supprimer les tables
drop table if exists List_Contains_Equipment;
drop table if exists Equipments_List;
drop table if exists Equipment;
drop table if exists Category;
drop table if exists Login_Ticket;
drop table if exists Users;


-- Table des utilisateurs
create table Users(
    user_email VARCHAR(255) primary key not null,
    professional BOOLEAN not null,
    password_hashed VARCHAR(255) not null,
    last_name VARCHAR(64),
    first_name VARCHAR(64),
    birth_date TIMESTAMP,
    photo_url TEXT
);

--	Ticket de connexion
create table Login_Ticket(
    login_ticket_id SERIAL primary key not null,
    begin_date TIMESTAMP not null,
    end_date TIMESTAMP not null,
    "key" VARCHAR(255) not null,
    user_email VARCHAR(255) not null,
    foreign key (user_email) references Users(user_email) on delete cascade
);

--	Categorie des equipements
create table Category(
    category_title VARCHAR(255) primary key not null,
    description TEXT
);

--	Les équipements
create table Equipment(
    category_title VARCHAR(255) not null,
    equipment_name VARCHAR(255) not null,
    average_life_duration FLOAT4,
    construction_footprint FLOAT4 not null,
    usage_footprint FLOAT4 not null,
    primary key (category_title, equipment_name),
    foreign key (category_title) references Category(category_title)
);

--	Liste des equipements
create table Equipments_List(
    equipments_list_id SERIAL primary key not null,
    user_email VARCHAR(255) not null,
    simulation BOOLEAN not null,
    "name" VARCHAR(255),
    creation_date TIMESTAMP not null,
    last_modification_date TIMESTAMP not null,
    foreign key (user_email) references Users(user_email) on delete cascade
);


--	Élément d'une liste d'équipements
create table List_Contains_Equipment(
    list_contains_equipment_id SERIAL not null primary key,
    equipments_list_id INT not null,
    category_title VARCHAR(255) not null,
    equipment_name VARCHAR(255) not null,
    usage_frequency VARCHAR(255),
    commission_date TIMESTAMP,
    quantity INT not null,
    foreign key (equipments_list_id)
        references Equipments_List(equipments_list_id) on delete cascade,
    foreign key (category_title, equipment_name)
        references Equipment(category_title, equipment_name) on delete cascade
);

--	Peuplement de la BDD
--	Peuplement de la table Category
insert into Category(category_title, description) values
    ('Télévision', 'Télévision.'),
    ('Laptop', 'Ordinateur portable.'),
    ('Ordinateur', 'Ordinateur de bureau.'),
    ('Ecran', 'Écran.'),
    ('Tablette', 'Tablette.'),
    ('Smartphone', 'Smartphone.'),
    ('Baladeur numérique', 'Baladeur numérique.'),
    ('Chaîne HiFi', 'Chaîne HiFi.'),
    ('Modem', 'Modem.'),
    ('Décodeur', 'Décodeur.'),
    ('Liseuse', 'Liseuse.'),
    ('Console vidéo', 'Console de jeu.'),
    ('Imprimante', 'Imprimante.'),
    ('Appareil photo', 'Appareil photo.'),
    ('Montre connectée', 'Montre connectée.'),
    ('Cadre photo électronique', 'Cadre photo électronique.'),
    ('Home cinéma', 'Home cinéma.'),
    ('Vidéo-projecteur', 'Vidéo-projecteur.'),
    ('Ecran publicitaire', 'Ecran publicitaire.'),
    ('Clavier', 'Clavier pour ordinateur.'),
    ('Souris', 'Souris pour ordinateur.'),
    ('Serveur', 'Serveur.');

--	Peuplement de la table equipment
insert into Equipment(
    category_title,
    equipment_name,
    average_life_duration,
    usage_footprint,
    construction_footprint
) values
    ('Télévision','Télévision < 40"',5,46,320),
    ('Télévision','Télévision 40-49"',5,53,350),
    ('Télévision','Télévision > 49"',5,71,466),
    ('Laptop','Laptop bureautique',4,42,124),
    ('Laptop','Laptop haute performance',4,48,135),
    ('Laptop','Laptop MacBook',4,48,165),
    ('Ordinateur','Ordinateur bureautique',4,32,94),
    ('Ordinateur','Ordinateur haute performance',4,66,169),
    ('Ordinateur','Ordinateur Mac Pro',4,70,450),
    ('Ecran','Ecran < 21"',5,36,211),
    ('Ecran','Ecran 21-31"',5,40,239),
    ('Ecran','Ecran > 32"',5,55,295),
    ('Tablette','Tablette classique (9 -11")',2,23,50),
    ('Tablette','Tablette mini (< 9")',1,16,31),
    ('Tablette','Tablettes détachables (10-13")',2,29,63),
    ('Smartphone','Smartphone basique',3,8,14),
    ('Smartphone','Smartphone < 4"',2,14,24),
    ('Smartphone','Smartphone 5"','1.5',16,27),
    ('Smartphone','Smartphone > 6"','1.5',19,33),
    ('Baladeur numérique','Baladeur numérique non tactile',5,1, 1.1),
    ('Baladeur numérique','Baladeur numérique tactile',3,3,6),
    ('Chaîne HiFi','Chaîne stéréo classique',7,27,98),
    ('Chaîne HiFi','Enceinte active Bluetooth',5,2,8),
    ('Modem','Modem DSL haut débit',6,24,58),
    ('Modem','Modem fibre haut débit',6,35,78),
    ('Décodeur','Décodeur',6,17,57),
    ('Liseuse','Liseuse non rétro-éclairée',3,7,29),
    ('Liseuse','Liseuse rétro-éclairée',3,9,36),
    ('Console vidéo','Console de salon',5,20,69),
    ('Console vidéo','Console portable',5,6,22),
    ('Imprimante','Imprimante jet d’encre',5,21,73),
    ('Imprimante','Imprimante laser',5,38,172),
    ('Imprimante','Imprimante Multi fonction jet d’encre',5,19,76),
    ('Appareil photo','Appareil photo reflex numérique',6,6,28),
    ('Appareil photo','Appareil photo hybride',6,5,27),
    ('Appareil photo','Appareil photo compact',4,4,24),
    ('Montre connectée','Montre connectée',3,2,4),
    ('Cadre photo électronique','Cadre photo électronique',3,8,39),
    ('Home cinéma','Home cinéma',5,31,106),
    ('Home cinéma','Barre de son',6,9,37),
    ('Vidéo-projecteur','Vidéo-projecteur',8,11,30),
    ('Ecran publicitaire','Ecran publicitaire',10,164,234),
    ('Clavier','Clavier',4,'0.1',24),
    ('Souris','Souris',4,'0.1',5),
    ('Serveur','Serveur classique',5,180,450),
    ('Serveur','Noeud de calcul',5,300,750);