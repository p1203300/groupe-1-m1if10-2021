const state = {
    categoriesTitles: [],
    categoriesDescriptions: [],
    equipmentsNames: [],
    equipmentsCategories: [],
    equipmentsAverageLifeDuration: [],
    equipmentsConstructionFootPrint: [],
    equipmentsUsageFootPrint: [],

    listNames: [],
    listIDs: [],
    listSimulations: [],
    categoryNumbers: [],
    equipmentsNumbers: [],
    equipmentsYears: [],
    equipmentsFrequencies: [],
    equipmentsQuantities: [],

    equipmentNumber: 0,
    constructionFootPrints: [],
    usageFootPrints: [],

    userConnected: false,
    url: ""
};

function initializeEquipments(userConnected, url, categoriesTitlesArray, categoriesDescriptionsArray, equipmentsNamesArray,
                              equipmentsCategoriesArray, equipmentsAverageLifeDurationArray,
                              equipmentsConstructionFootPrintArray, equipmentsUsageFootPrintArray) {
    console.debug('@initializeEquipments');
    state.userConnected = userConnected;
    state.url = url;
    state.categoriesTitles = categoriesTitlesArray;
    state.categoriesDescriptions = categoriesDescriptionsArray;
    state.equipmentsNames = equipmentsNamesArray;
    state.equipmentsCategories = equipmentsCategoriesArray;
    state.equipmentsAverageLifeDuration = equipmentsAverageLifeDurationArray;
    state.equipmentsConstructionFootPrint = equipmentsConstructionFootPrintArray;
    state.equipmentsUsageFootPrint = equipmentsUsageFootPrintArray;

    console.debug(state.userConnected);
    console.debug(state.url);
    console.debug(state.categoriesTitles);
    console.debug(state.categoriesDescriptions);
    console.debug(state.equipmentsNames);
    console.debug(state.equipmentsCategories);
    console.debug(state.equipmentsAverageLifeDuration);
    console.debug(state.equipmentsConstructionFootPrint);
    console.debug(state.equipmentsUsageFootPrint);

    renderEquipmentList();
}

function renderEquipmentList() {
    console.debug('@renderEquipmentList');

    let html = '<label>Nom de la liste d\'équipements : ';
    html += '<div id="loginTicket"></div>';
    html += '<div id="NameError"></div>';
    html += '<input type="text" placeholder="Mes équipements" id="listName" name="listName"/>';
    html += '<div id="NumberError"></div>';
    html += '</label>';

    html += '<input type="hidden" value="0" id="equipmentsNumber" name="equipmentsNumber"/>';
    html += '<input type="hidden" value="-1" id="listNumber" name="listNumber"/>';
    html += '<input type="hidden" value="0" id="isSimulation" name="isSimulation"/>';

    html += '<div id="CategorySelectionDiv' + (state.equipmentNumber + 1) + '"></div>';
    html += '<input type="button" onclick=addNewEquipment(' + (state.equipmentNumber + 1) + ') value="Ajouter un équipement" id="addButton' + (state.equipmentNumber + 1) + '" />';
    html += '<div id="TotalFootPrint"></div>';
    html += '<input type="button" value="Enregistrer la liste" id="submitButton" onclick="verifySubmit()" />';

    document.getElementById("EquipmentListDiv").innerHTML = html;
    let listName = document.getElementById("listName");
    listName.addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
        }
    });
    renderCategorySelection();
}

function addNewEquipment(number) {
    let button = document.getElementById("addButton" + number);
    button.parentNode.removeChild(button);
    let submit = document.getElementById("submitButton");
    submit.parentNode.removeChild(submit);
    let footPrint = document.getElementById("TotalFootPrint");
    let footPrintContent = footPrint.innerHTML;
    footPrint.parentNode.removeChild(footPrint);
    let html = '<div id="CategorySelectionDiv' + (number + 1) + '"></div>';
    html += '<input type="button" onclick=addNewEquipment(' + (number + 1) + ') value="Ajouter un équipement" id="addButton' + (number + 1) + '" />';
    html += '<div id="TotalFootPrint">' + footPrintContent + '</div>';
    html += '<input type="button" value="Enregistrer la liste" id="submitButton" onclick="verifySubmit()" />';
    let container = document.createElement("DivEquipment" + (number + 1));
    container.id = "DivEquipment" + (number + 1);
    container.innerHTML = html;
    document.getElementById("EquipmentListDiv").appendChild(container);
    renderCategorySelection();
}

function renderCategorySelection() {
    console.debug('@renderCategorySelection');
    state.equipmentNumber++;
    document.getElementById("equipmentsNumber").value = state.equipmentNumber;
    let html = '<h3 id="title' + state.equipmentNumber + '">Equipement n°' + state.equipmentNumber + '</h3>';
    html += '<input type="button" id="delete' + state.equipmentNumber + '" value="Supprimer cet équipement" onclick="deleteEquipment(' + state.equipmentNumber + ')" /><br />';
    html += '<div id="CategoryError' + state.equipmentNumber + '"></div>';
    html += '<label>';
    html += 'Sélectionnez une catégorie : ';
    html += '<select name="category' + state.equipmentNumber + '" id="category' + state.equipmentNumber + '" onchange="renderEquipmentSelection(' + state.equipmentNumber + ')">';
    html += '<option value="/"></option>';
    for (let i = 0; i < state.categoriesTitles.length; i++) {
        html += '<option value="' + i + '">' + state.categoriesTitles[i] + '</option>';
    }
    html += '</select>';
    html += '</label>';
    html += '<div id="EquipmentSelectionDiv' + state.equipmentNumber + '"></div>';
    document.getElementById("CategorySelectionDiv" + state.equipmentNumber).innerHTML = html;
}

function renderEquipmentSelection(number) {
    console.debug('@renderEquipmentSelection');
    let category = document.getElementById("category" + number).value;
    if(category == "/") {
        document.getElementById('EquipmentSelectionDiv' + number).innerHTML = '';
        renderEquipmentProperties(number);
        return;
    }
    let html = '<div id="EquipmentError' + number + '"></div>';
    html += '<label>';
    html += 'Sélectionnez un équipement : ';
    html += '<select name="equipment' + number + '" id="equipment' + number + '" onchange="renderEquipmentProperties(' + number + ')">';
    html += '<option value="/"></option>';
    for (let i = 0; i < state.equipmentsNames[category].length; i++) {
        html += '<option value="' + i + '">' + state.equipmentsNames[category][i] + '</option>';
    }
    html += '</select>';
    html += '</label>';
    html += '<div id="EquipmentPropertyDiv' + number + '"></div>';
    document.getElementById('EquipmentSelectionDiv' + number).innerHTML = html;
}

function renderEquipmentProperties(number) {
    console.debug('@renderEquipmentProperties');
    let category = document.getElementById("category" + number).value;
    let equipment = "/";
    if(document.getElementById("equipment" + number) != null) {
        equipment = document.getElementById("equipment" + number).value;
    }
    if(category == "/" || equipment == "/") {
        if(document.getElementById("EquipmentPropertyDiv" + number) != null) {
            document.getElementById("EquipmentPropertyDiv" + number).innerHTML = "";
        }
        renderEquipmentFootPrint(number);
        return;
    }
    let html = '<input type="hidden" id="equipmentName' + number + '" name="equipmentName' + number + '" value=\'' + state.equipmentsNames[category][equipment] + '\' />';
    html += '<input type="hidden" id="categoryTitle' + number + '" name="categoryTitle' + number + '" value=\'' + state.categoriesTitles[category] + '\' />';
    html += '<p>Durée de vie moyenne : ';
    html += state.equipmentsAverageLifeDuration[category][equipment];
    html += ' ; Empreinte carbone (construction) : ';
    html += state.equipmentsConstructionFootPrint[category][equipment];
    html += ' ; Empreinte carbone (utilisation) : ';
    html += state.equipmentsUsageFootPrint[category][equipment];
    html +='</p><br />';
    html += '<div id="YearError' + number + '"></div>';
    html += '<label>';
    html += 'Année d\'achat de l\'équipement : ';
    html += '<select name="year' + number + '" id="year' + number + '" onchange="renderEquipmentFootPrint(' + number + ')">';
    let year = new Date().getFullYear();
    for(let i = year; i >= 1980; i--) {
        html += '<option value="' + i + '">' + i + '</option>';
    }
    html += '</select>';
    html += '</label>';
    html += '<div id="FrequencyError' + number + '"></div>';
    html += '<label>';
    html += 'Fréquence d\'utilisation : ';
    html += '<select name="frequency' + number + '" id="frequency' + number + '" onchange="renderEquipmentFootPrint(' + number + ')">';
    html += '<option value="4">Haute</option>';
    html += '<option value="2" selected>Normale</option>';
    html += '<option value="1">Faible</option>';
    html += '</select>';
    html += '</label>';
    html += '<div id="QuantityError' + number + '"></div>';
    html += '<label>';
    html += 'Quantité : ';
    html += '<input type="number" id="quantity' + number + '" name="quantity' + number + '" min="0" value="1" onchange="renderEquipmentFootPrint(' + number + ')" />';
    html += '</label>';
    html += '<div id="EquipmentFootPrintDiv' + number + '"></div>';
    document.getElementById("EquipmentPropertyDiv" + number).innerHTML = html;
    renderEquipmentFootPrint(number);
}

function renderEquipmentFootPrint(number) {
    console.debug('@renderEquipmentFootPrint');
    let html = '<p>Empreinte de construction :';
    let category = document.getElementById("category" + number).value;
    let equipment = "/";
    if(document.getElementById("equipment" + number) != null) {
        equipment = document.getElementById("equipment" + number).value;
    }
    if(category == "/" || equipment == "/") {
        if(document.getElementById("EquipmentFootPrintDiv" + number) != null) {
            document.getElementById("EquipmentFootPrintDiv" + number).innerHTML = "";
        }
        state.constructionFootPrints[number - 1] = 0;
        state.usageFootPrints[number - 1] = 0;
        renderTotalFootPrint();
        return;
    }
    let year = document.getElementById("year" + number).value;
    let frequency = document.getElementById("frequency" + number).value * 0.5;
    let quantity = document.getElementById("quantity" + number).value;
    if(quantity == null || !isInt(quantity) || quantity < 0) {
        quantity = 1;
        document.getElementById("quantity" + number).value = 1;
    }
    if (isInt(quantity) && isInt(year) && isNumber(frequency)) {
        state.constructionFootPrints[number - 1] = state.equipmentsConstructionFootPrint[category][equipment] * quantity;
        html += state.constructionFootPrints[number - 1];
        html += ' ; Empreinte d\'utilisation : ';
        let currentYear = new Date().getFullYear();
        state.usageFootPrints[number - 1] = state.equipmentsUsageFootPrint[category][equipment] * quantity * frequency * (currentYear - year + 1);
        html += state.usageFootPrints[number - 1];
        document.getElementById("EquipmentFootPrintDiv" + number).innerHTML = html;
    }
    else {
        state.constructionFootPrints[number - 1] = 0;
        state.usageFootPrints[number - 1] = 0;
        document.getElementById("EquipmentFootPrintDiv" + number).innerHTML = "";
    }

    renderTotalFootPrint();
}

function renderTotalFootPrint() {
    console.debug('@renderTotalFootPrint');
    let constructionFootPrint = 0;
    let usageFootPrint = 0;
    if(state.equipmentNumber == 0) {
        document.getElementById("TotalFootPrint").innerHTML = "";
        return;
    }
    for(let i = 0; i < state.equipmentNumber; i++) {
        constructionFootPrint += state.constructionFootPrints[i];
        usageFootPrint += state.usageFootPrints[i];
    }
    if(isNumber(constructionFootPrint) && isNumber(usageFootPrint) && constructionFootPrint > 0 && usageFootPrint > 0) {
        let html = '<p>Empreinte carbone de construction totale : <strong>';
        html += constructionFootPrint + '</strong></p>';
        html += '<p>Empreinte carbone d\'utilisation totale : <strong>';
        html += usageFootPrint + '</strong></p>';
        html += '<p>Empreinte carbone totale : <strong>' + (constructionFootPrint + usageFootPrint) + '</strong></p>';
        document.getElementById("TotalFootPrint").innerHTML = html;
    } else {
        document.getElementById("TotalFootPrint").innerHTML = "";
    }
}

function deleteEquipment(number) {
    console.debug('@deleteEquipment');
    let div = document.getElementById("CategorySelectionDiv" + number);
    div.parentNode.removeChild(div);
    let divEquip = document.getElementById("CategorySelectionDiv" + number);
    if (divEquip !== null) {
        divEquip.parentNode.removeChild(divEquip);
    }
    for (let i = 1; i <= state.equipmentNumber; i++) {
        if (i > number) {
            document.getElementById("CategorySelectionDiv" + i).id = "CategorySelectionDiv" + (i - 1);
            document.getElementById("DivEquipment" + i).id = "DivEquipment" + (i - 1);
            document.getElementById("title" + i).innerHTML = "Equipement n°" + (i - 1);
            document.getElementById("title" + i).id = "title" + (i - 1);
            document.getElementById("delete" + i).onclick = function () {
                deleteEquipment(i - 1);
            };
            document.getElementById("delete" + i).id = "delete" + (i - 1);
            document.getElementById("CategoryError" + i).id = "CategoryError" + (i - 1);
            document.getElementById("category" + i).onchange = function () {
                renderEquipmentSelection(i - 1);
            };
            document.getElementById("category" + i).name = "category" + (i - 1);
            document.getElementById("category" + i).id = "category" + (i - 1);
            document.getElementById("EquipmentSelectionDiv" + i).id = "EquipmentSelectionDiv" + (i - 1);
            if (document.getElementById("category" + (i - 1)).value !== "/") {
                document.getElementById("EquipmentError" + i).id = "EquipmentError" + (i - 1);
                document.getElementById("equipment" + i).onchange = function () {
                    renderEquipmentProperties(i - 1);
                };
                document.getElementById("equipment" + i).name = "equipment" + (i - 1);
                document.getElementById("equipment" + i).id = "equipment" + (i - 1);
                document.getElementById("EquipmentPropertyDiv" + i).id = "EquipmentPropertyDiv" + (i - 1);
                if (document.getElementById("equipment" + (i - 1)).value !== "/") {
                    document.getElementById("equipmentName" + i).name = "equipmentName" + (i - 1);
                    document.getElementById("equipmentName" + i).id = "equipmentName" + (i - 1);
                    document.getElementById("categoryTitle" + i).name = "categoryTitle" + (i - 1);
                    document.getElementById("categoryTitle" + i).id = "categoryTitle" + (i - 1);
                    document.getElementById("YearError" + i).id = "YearError" + (i - 1);
                    document.getElementById("year" + i).onchange = function () {
                        renderEquipmentFootPrint(i - 1);
                    };
                    document.getElementById("year" + i).name = "year" + (i - 1);
                    document.getElementById("year" + i).id = "year" + (i - 1);
                    document.getElementById("FrequencyError" + i).id = "FrequencyError" + (i - 1);
                    document.getElementById("frequency" + i).onchange = function () {
                        renderEquipmentFootPrint(i - 1);
                    };
                    document.getElementById("frequency" + i).name = "frequency" + (i - 1);
                    document.getElementById("frequency" + i).id = "frequency" + (i - 1);
                    document.getElementById("QuantityError" + i).id = "QuantityError" + (i - 1);
                    document.getElementById("quantity" + i).onchange = function () {
                        renderEquipmentFootPrint(i - 1);
                    };
                    document.getElementById("quantity" + i).name = "quantity" + (i - 1);
                    document.getElementById("quantity" + i).id = "quantity" + (i - 1);
                    document.getElementById("EquipmentFootPrintDiv" + i).id = "EquipmentFootPrintDiv" + (i - 1);
                }
            }
        }
    }
    let total = state.equipmentNumber;
    document.getElementById("addButton" + state.equipmentNumber).onclick = function () {
        addNewEquipment(total - 1);
    };
    document.getElementById("addButton" + state.equipmentNumber).id = "addButton" + (state.equipmentNumber - 1);
    state.equipmentNumber--;
    document.getElementById("equipmentsNumber").value = state.equipmentNumber;
    renderTotalFootPrint();
}

// Modifying list functions

function initializeLists(listNamesArray, listIDsArray, listSimulationsArray, categoryNumberArray, equipmentsNumberArray,
                         equipmentsYearsArray, equipmentsFrequenciesArray, equipmentsQuantitiesArray) {
    console.debug('@initializeLists');
    state.listNames = listNamesArray;
    state.listIDs = listIDsArray;
    state.listSimulations = listSimulationsArray;
    state.categoryNumbers = categoryNumberArray;
    state.equipmentsNumbers = equipmentsNumberArray;
    state.equipmentsYears = equipmentsYearsArray;
    state.equipmentsFrequencies = equipmentsFrequenciesArray;
    state.equipmentsQuantities = equipmentsQuantitiesArray;

    console.debug(state.listNames);
    console.debug(state.listIDs);
    console.debug(state.listSimulations);
    console.debug(state.categoryNumbers);
    console.debug(state.equipmentsNumbers);
    console.debug(state.equipmentsYears);
    console.debug(state.equipmentsFrequencies);
    console.debug(state.equipmentsQuantities);
}

function setList(listNumber) {
    console.debug('@setList');
    simulation = false;
    OK = true;
    if (listNumber == -1) {
        listNumber = document.getElementById("list").value;
        simulation = true;
        let pass = false;
        if(state.equipmentNumber == 0 || (state.equipmentNumber == 1 && document.getElementById("category1").value == "/")) {
            pass = true;
        }
        console.debug(pass);
        if (pass || confirm("Voulez-vous vraiment charger la liste " + state.listNames[listNumber] + " ? Toute votre saisie sera écrasée.")) {
            state.equipmentNumber = 0;
            document.getElementById("EquipmentListDiv").innerHTML = '';
            renderEquipmentList();
            document.getElementById("isSimulation").value = "1";
        } else {
            OK = false;
        }
        document.getElementById("list").value = "/";
    }
    if (listNumber !== "/" && OK) {
        if (!simulation) {
            document.getElementById("listNumber").value = state.listIDs[listNumber];
            document.getElementById("RestoreDiv").innerHTML = '<input type="button" id="restoreButton" value="Annuler les modifications" onclick="restoreList(' + listNumber + ')" /><br /><br />';
        }
        document.getElementById("listName").value = state.listNames[listNumber];
        for (let i = 0; i < state.categoryNumbers[listNumber].length; i++) {
            document.getElementById("category" + (i + 1)).value = state.categoryNumbers[listNumber][i];
            renderEquipmentSelection(i + 1);
            document.getElementById("equipment" + (i + 1)).value = state.equipmentsNumbers[listNumber][i];
            renderEquipmentProperties(i + 1);
            document.getElementById("year" + (i + 1)).value = state.equipmentsYears[listNumber][i];
            document.getElementById("frequency" + (i + 1)).value = state.equipmentsFrequencies[listNumber][i];
            document.getElementById("quantity" + (i + 1)).value = state.equipmentsQuantities[listNumber][i]
            renderEquipmentFootPrint(i + 1);
            if (i < state.categoryNumbers[listNumber].length - 1) {
                addNewEquipment(i + 1);
            }
        }
        renderTotalFootPrint();
    }
    if (state.listSimulations[listNumber] == "true" || simulation) {
        document.getElementById("submitButton").value = "Enregistrer la simulation";
    }
}

function restoreList(listNumber) {
    console.debug('@restoreList');
    if (confirm("Voulez-vous vraiment supprimer les modifications effectuées ?")) {
        simulation = false;
        state.equipmentNumber = 0;
        document.getElementById("EquipmentListDiv").innerHTML = '';
        renderEquipmentList();
        if (state.listSimulations[listNumber] == "true") {
            document.getElementById("list").value = "/";
            simulation = true;
            document.getElementById("isSimulation").value = "1";
        }
        if (!simulation) {
            document.getElementById("listNumber").value = state.listIDs[listNumber];
        }
        document.getElementById("listName").value = state.listNames[listNumber];
        for (let i = 0; i < state.categoryNumbers[listNumber].length; i++) {
            document.getElementById("category" + (i + 1)).value = state.categoryNumbers[listNumber][i];
            renderEquipmentSelection(i + 1);
            document.getElementById("equipment" + (i + 1)).value = state.equipmentsNumbers[listNumber][i];
            renderEquipmentProperties(i + 1);
            document.getElementById("year" + (i + 1)).value = state.equipmentsYears[listNumber][i];
            document.getElementById("frequency" + (i + 1)).value = state.equipmentsFrequencies[listNumber][i];
            document.getElementById("quantity" + (i + 1)).value = state.equipmentsQuantities[listNumber][i]
            renderEquipmentFootPrint(i + 1);
            if (i < state.categoryNumbers[listNumber].length - 1) {
                addNewEquipment(i + 1);
            }
        }
        renderTotalFootPrint();
        if (state.listSimulations[listNumber] == "true") {
            document.getElementById("submitButton").value = "Enregistrer la simulation";
        }
    }
}

// Simulation functions

function selectList(listNames, listIDs) {
    console.debug('@setList');
    document.getElementById("isSimulation").value = "1";
    let html = '<label>';
    html += 'Vous pouvez partir d\'une de vos listes : ';
    html += '<select name="list" id="list" onchange="setList(-1)">';
    html += '<option value="/"></option>';
    for (let i = 0; i < listNames.length; i++) {
        if (state.listSimulations[i] == "false") {
            html += '<option value="' + i + '">' + listNames[i] + '</option>';
        }
    }
    html += '</select>';
    html += '</label>';
    document.getElementById("SimulationDiv").innerHTML = html;
    document.getElementById("submitButton").value = "Enregistrer la simulation";
}

// Submit functions

function verifySubmit() {
    console.debug('@verifySubmit');
    let pass = true;

    // List Name
    let name = document.getElementById("listName").value;
    if (name == null || name === "") {
        document.getElementById("NameError").innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir un nom de liste.</p>';
        pass = false;
    } else {
        document.getElementById("NameError").innerHTML = "";
    }

    if (state.equipmentNumber == 0) {
        document.getElementById("NameError").innerHTML = '<p style="color:red;font-style:italic;"> Il vous faut au moins un équipement.</p>';
        pass = false;
    }

    // Equipments
    for (let i = 1; i <= state.equipmentNumber; i++) {
        let category = document.getElementById("category" + i).value;
        if (category == null || category === "/") {
            document.getElementById('CategoryError' + i).innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir une catégorie.</p>';
            pass = false;
        } else {
            document.getElementById('CategoryError' + i).innerHTML = "";
            let equipment = document.getElementById("equipment" + i).value;
            if (equipment == null || equipment == "/") {
                document.getElementById('EquipmentError' + i).innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir un équipement.</p>';
                pass = false;
            } else {
                document.getElementById('EquipmentError' + i).innerHTML = "";
                let year = document.getElementById("year" + i).value;
                let frequency = document.getElementById("frequency" + i).value;
                let quantity = document.getElementById("quantity" + i).value;
                let actualYear = new Date().getFullYear();
                if (year == null || !isInt(year) || year > actualYear || year < 1980) {
                    document.getElementById("YearError" + i).innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir une année comprise entre 1980 et ' + actualYear + '.</p>';
                    pass = false;
                } else {
                    document.getElementById("YearError" + i).innerHTML = "";
                }
                if (frequency == null || !isInt(frequency) || frequency < 1 || frequency > 4 || frequency == 3) {
                    document.getElementById("FrequencyError" + i).innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir une fréquence d\'utilisation valide.</p>';
                    pass = false;
                } else {
                    document.getElementById("FrequencyError" + i).innerHTML = "";
                }
                if (quantity == null || !isInt(quantity) || quantity < 0) {
                    document.getElementById("QuantityError" + i).innerHTML = '<p style="color:red;font-style:italic;"> Veuillez saisir une quantité entière positive.</p>';
                    pass = false;
                } else {
                    document.getElementById("QuantityError" + i).innerHTML = "";
                }
            }
        }
    }
    if (pass) {
        if (!state.userConnected) {
            var win = window.open(state.url, 'Connectez-vous', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1200,height=850');
            var timer = setInterval(function () {
                win.document.title = 'Connectez-vous';
                win.onload = function () {
                    let html = "<input type=\"hidden\" id=\"userNotConnected\" value=\"false\" name=\"userNotConnected\" />";
                    if (win.document.getElementById("loginForm") != null) {
                        win.document.getElementById("loginForm").insertAdjacentHTML('afterbegin', html);
                        win.document.getElementById("goToRegisterForm").insertAdjacentHTML('afterbegin', html);

                    }
                };
                if (win.closed) {
                    clearInterval(timer);
                    let connected = getUserConnected();
                    if (connected) {
                        alert('Vous avez bien été connecté.');
                        document.getElementById('loginTicket').innerHTML = '<input type="hidden" name="createdLoginTicket" id="createdLoginTicket" value="'
                            + document.getElementById("loginTicketId").value + '" />';
                        state.userConnected = true;
                        document.getElementById("listForm").submit();
                    } else {
                        document.getElementById('loginTicket').innerHTML = '';
                        alert('Vous n\'avez pas été correctement connecté. Veuillez réessayer.');
                    }
                }
            }, 0);
        } else {
            document.getElementById("listForm").submit();
        }
    }
}

function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    let x = parseFloat(value);
    return (x | 0) === x;
}

function isNumber(value) {
    if (isNaN(value)) {
        return false;
    }
    return (parseFloat(value) === value);
}