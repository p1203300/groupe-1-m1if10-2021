<html>
<head>
    <title>Carbone calculator - Accueil</title>
    <link rel="stylesheet" type="text/css" href="static/home.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>

</head>
<body>
<h1>Carbone calculator</h1>
<br>
<div id="carbon_calculator_box">
    <h2>Description de l'application</h2>
    <p>
        En raison du changement climatique, il devient nécessaire de calculer l'empreinte carbone réalisée par chacun
        afin d'améliorer la santé de tous.
    </p>
    <p>
        Carbone calculator est une application permettant de calculer ce poids pour une liste d'équipements
        informatiques à la fois pour les particuliers mais aussi pour les entreprises.
    </p>
</div>
<br><br><br><br>
<div class="div_btn">
    <form action="carbon/nouvelleListe" method="get">
        <input class="wrapper" type="submit" id="home_button" value="Calculer son empreinte carbone">
    </form>
</div>
<br><br><br><br>


<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
