<!doctype html>
<html lang="fr">
<head>
    <title>Se connecter</title>
    <link rel="stylesheet" type="text/css" href="../static/registration.css">
    <link rel="icon" type="image/png" href="../images/logo.png"/>
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>

<div id="registration_box">
    <div id="connexionForm">
        <form id="loginForm" name="login" action="seConnecter" method="post">
            <h2>Carbon Calculator</h2>
            <p>
                Votre email :<br/>
                <input type="text" name="email" id="email" maxlength="255"
                       pattern="[a-zA-Z][a-zA-Z0-9.-]+@[a-zA-Z0-9][a-zA-Z0-9.-]+[a-zA-Z0-9]+" required/>
                <%
                    if (request.getAttribute("user_not_found") != null) {
                        out.print("Utilisateur inexistant");
                    }
                    if (request.getAttribute("invalid_email") != null) {
                        out.print("le format de l'email est invalide");
                    }
                %>
                </p>
            </p>

            <p>
                Votre mot de passe :<br/>
                <input type="password" name="password" minlength="8" maxlength="255" required/>
            <p class="incorect">
                <%
                    if (request.getAttribute("invalid_password") != null) {
                        out.print("Mot de passe incorrect");
                    }
                %>
            </p>

            <p>
                <input type="submit" value="Se connecter"/>
            </p>
        </form>
        <form name="goToRegister" id="goToRegisterForm" action="nouveauCompte" method="post">
            <input id="sinscrire" type="submit" value="S'inscrire"/>
        </form>
    </div>
</div>
<br><br><br><br>

<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>