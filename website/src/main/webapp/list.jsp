<%@ page import="java.util.List" %>
<%@ page import="fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="fr.univlyon1.m1if.m1if10.g1.modele.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%
    WebSiteCarbon wbc = (WebSiteCarbon) pageContext.getServletContext().getAttribute("webSiteCarbon");
%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Carbon calculator - Enregistrer une liste</title>
        <link rel="stylesheet" type="text/css" href="../static/profil.css">
        <%
            boolean simulation = false;
            int listID = -1;
            String message = "";
            if(request.getParameter("listNumber") == null && request.getAttribute("simulation") == null) {
                message = "<h2>Créer une nouvelle liste</h2>";
            }
            else if(request.getAttribute("simulation") != null && request.getParameter("listNumber") == null) {
                simulation = true;
                message = "<h2>Créer une nouvelle simulation</h2>";
            }
            else if(request.getAttribute("simulation") == null && request.getParameter("listNumber") != null) {
                listID = Integer.parseInt(request.getParameter("listNumber"));
                message = "<h2>Modifier une liste</h2>";
            }
            else {
                simulation = true;
                listID = Integer.parseInt(request.getParameter("listNumber"));
                message = "<h2>Modifier une simulation</h2>";
            }
        %>
        <script type="text/javascript" defer>
            function getUserConnected() {
                return document.getElementById("loginTicketId") != null;
            }
        </script>
    </head>
    <body>
    <%@include file="WEB-INF/components/headerPage.jsp" %>
            <%=message%>
            <form method="post" action="enregistrerListe" name="listForm" id="listForm">
            <div id="SimulationDiv"></div>
            <div id="RestoreDiv"></div>
            <div id="EquipmentListDiv"></div>
        </form>

    <br><br><br><br><br>
    <%@include file="WEB-INF/components/footer.jsp" %>
    </body>
</html>
<script type="text/javascript" src="../js/list.js"></script>
<script type="text/javascript" defer>
    <%
        // Init category list and equipments lists
        List<Category> categoriesList = wbc.getCategoryList();
        List<List<Equipment>> equipmentsList = new ArrayList<>();
        List<String> categoriesTitlesList = new ArrayList<>();
        List<String> categoriesDescriptionsList = new ArrayList<>();
        List<List<String>> equipmentsNamesList = new ArrayList<>();
        List<List<String>> equipmentsCategoriesList = new ArrayList<>();
        List<List<Float>> equipmentsAverageLifeDurationList = new ArrayList<>();
        List<List<Float>> equipmentsConstructionFootPrintList = new ArrayList<>();
        List<List<Float>> equipmentsUsageFootPrintList = new ArrayList<>();
        categoriesList.forEach(category -> {
            categoriesTitlesList.add(category.getTitle());
            categoriesDescriptionsList.add(category.getDescription());
            List<Equipment> equipmentList = wbc.getEquipmentsByCategory(category.getTitle());
            equipmentsList.add(equipmentList);
            List<String> names = new ArrayList<>();
            List<String> titles = new ArrayList<>();
            List<Float> averageLife = new ArrayList<>();
            List<Float> constructionFootPrint = new ArrayList<>();
            List<Float> usageFootPrint = new ArrayList<>();
            equipmentList.forEach(equipment -> {
                names.add(equipment.getName());
                titles.add(equipment.getCategory().getTitle());
                averageLife.add(equipment.getAverageLifeDuration());
                constructionFootPrint.add(equipment.getConstructionFootPrint());
                usageFootPrint.add(equipment.getUsageFootPrint());
            });
            equipmentsNamesList.add(names);
            equipmentsCategoriesList.add(titles);
            equipmentsAverageLifeDurationList.add(averageLife);
            equipmentsConstructionFootPrintList.add(constructionFootPrint);
            equipmentsUsageFootPrintList.add(usageFootPrint);
        });

        boolean isUserConnected = wbc.isUserConnected(request);

        List<String> listNames = new ArrayList<>();
        List<Integer> listIDs = new ArrayList<>();
        List<Boolean> listSimulations = new ArrayList<>();
        List<List<Integer>> categoryNumbers = new ArrayList<>();
        List<List<Integer>> equipmentsNumbers = new ArrayList<>();
        List<List<Integer>> equipmentsYears = new ArrayList<>();
        List<List<Integer>> equipmentsFrequencies = new ArrayList<>();
        List<List<Integer>> equipmentsQuantities = new ArrayList<>();

        if(isUserConnected) {
            String email = wbc.getCurrentUser(request).getEmail();
            List<EquipmentList> lists = wbc.getEquipmentListByUserEmail(email);
            lists.sort(EquipmentList.comparator);
            for(int i = 0 ; i < lists.size(); i++) {
                listNames.add(lists.get(i).getName());
                listIDs.add(lists.get(i).getId());
                listSimulations.add(lists.get(i).isSimulation());
                List<EquipmentListElement> elements = wbc.getEquipmentListElementsByListId(listIDs.get(i));
                List<Integer> categories = new ArrayList<>();
                List<Integer> equipments = new ArrayList<>();
                List<Integer> years = new ArrayList<>();
                List<Integer> frequencies = new ArrayList<>();
                List<Integer> quantities = new ArrayList<>();
                for(int j = 0 ; j < elements.size(); j++) {
                    int categoryNumber = categoriesTitlesList.indexOf(elements.get(j).getEquipement().getCategory().getTitle());
                    categories.add(categoryNumber);
                    equipments.add(equipmentsNamesList.get(categoryNumber).indexOf(elements.get(j).getEquipement().getName()));
                    Calendar calendar = Calendar.getInstance();
                    Date date = elements.get(j).getCommissionDate();
                    calendar.setTime(date);
                    years.add(calendar.get(Calendar.YEAR));
                    frequencies.add(Integer.parseInt(elements.get(j).getUsageFrequency()));
                    quantities.add(elements.get(j).getQuantity());
                }
                categoryNumbers.add(categories);
                equipmentsNumbers.add(equipments);
                equipmentsYears.add(years);
                equipmentsFrequencies.add(frequencies);
                equipmentsQuantities.add(quantities);
            }
        }

        StringBuffer currentURL = request.getRequestURL();
        String URL = currentURL.substring(0, currentURL.indexOf("carbon/") + "carbon/".length()) + "connexion";
        if(URL.contains(":8080/") && !URL.contains("localhost")) {
            URL = URL.substring(0, URL.indexOf(":8080/")) + "/website/carbon/connexion";
        }
    %>
    let userConnected = <%= isUserConnected %>;
    let url = '<%= URL %>';
    let categoriesTitlesArray = [];
    let categoriesDescriptionsArray = [];
    let equipmentsNamesArray = [];
    let equipmentsCategoriesArray = [];
    let equipmentsAverageLifeDurationArray = [];
    let equipmentsConstructionFootPrintArray = [];
    let equipmentsUsageFootPrintArray = [];
    <% for (int i=0; i < categoriesTitlesList.size(); i++) { %>
    categoriesTitlesArray[<%= i %>] = '<%= categoriesTitlesList.get(i) %>';
    categoriesDescriptionsArray[<%= i %>] = '<%= categoriesDescriptionsList.get(i) %>';
    equipmentsNamesArray[<%= i %>] = [];
    equipmentsCategoriesArray[<%= i %>] = [];
    equipmentsAverageLifeDurationArray[<%= i %>] = [];
    equipmentsConstructionFootPrintArray[<%= i %>] = [];
    equipmentsUsageFootPrintArray[<%= i %>] = [];
    <% for (int j = 0; j < equipmentsNamesList.get(i).size(); j++) { %>
    equipmentsNamesArray[<%= i %>][<%= j %>] = '<%= equipmentsNamesList.get(i).get(j) %>';
    equipmentsCategoriesArray[<%= i %>][<%= j %>] = '<%= equipmentsCategoriesList.get(i).get(j) %>';
    equipmentsAverageLifeDurationArray[<%= i %>][<%= j %>] = '<%= equipmentsAverageLifeDurationList.get(i).get(j) %>';
    equipmentsConstructionFootPrintArray[<%= i %>][<%= j %>] = '<%= equipmentsConstructionFootPrintList.get(i).get(j) %>';
    equipmentsUsageFootPrintArray[<%= i %>][<%= j %>] = '<%= equipmentsUsageFootPrintList.get(i).get(j) %>';
    <% } %>
    <% } %>

    initializeEquipments(userConnected, url, categoriesTitlesArray, categoriesDescriptionsArray, equipmentsNamesArray, equipmentsCategoriesArray,
        equipmentsAverageLifeDurationArray, equipmentsConstructionFootPrintArray, equipmentsUsageFootPrintArray);

    <% if (simulation || listID > -1) { %>
    let listNamesArray = [];
    let listIDsArray = [];
    let listSimulationsArray = [];
    let categoryNumbersArray = [];
    let equipmentsNumbersArray = [];
    let equipmentsYearsArray = [];
    let equipmentsFrequenciesArray = [];
    let equipmentsQuantitiesArray = [];
    <% for (int i = 0; i < categoryNumbers.size(); i++) { %>
    listNamesArray[<%= i %>] = '<%= listNames.get(i) %>';
    listIDsArray[<%= i %>] = '<%= listIDs.get(i) %>';
    listSimulationsArray[<%= i %>] = '<%= listSimulations.get(i) %>';
    categoryNumbersArray[<%= i %>] = [];
    equipmentsNumbersArray[<%= i %>] = [];
    equipmentsYearsArray[<%= i %>] = [];
    equipmentsFrequenciesArray[<%= i %>] = [];
    equipmentsQuantitiesArray[<%= i %>] = [];
    <% for (int j = 0; j < categoryNumbers.get(i).size(); j++) { %>
    categoryNumbersArray[<%= i %>][<%= j %>] = '<%= categoryNumbers.get(i).get(j) %>';
    equipmentsNumbersArray[<%= i %>][<%= j %>] = '<%= equipmentsNumbers.get(i).get(j) %>';
    equipmentsYearsArray[<%= i %>][<%= j %>] = '<%= equipmentsYears.get(i).get(j) %>';
    equipmentsFrequenciesArray[<%= i %>][<%= j %>] = '<%= equipmentsFrequencies.get(i).get(j) %>';
    equipmentsQuantitiesArray[<%= i %>][<%= j %>] = '<%= equipmentsQuantities.get(i).get(j) %>';
    <% } %>
    <% } %>

    initializeLists(listNamesArray, listIDsArray, listSimulationsArray, categoryNumbersArray, equipmentsNumbersArray,
        equipmentsYearsArray, equipmentsFrequenciesArray, equipmentsQuantitiesArray);

    <% if (simulation) { %>
    selectList(listNamesArray, listIDsArray);
    <% } %>

    <% if (listID > -1) { %>
    <% int listNumber = listIDs.indexOf(listID); %>
    setList(<%= listNumber %>);
    <% } %>

    <% } %>

</script>
