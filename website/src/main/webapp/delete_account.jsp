<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form name="delete_account" action="deleteAccount" method="post">
      <p>
        Votre email :<br/>
        <input type="text" name="email" required/>
        <% 
          if (request.getAttribute("user_not_found") != null) {
            out.print("utilisateur inexistant");
          } 
        %>
      </p>
      
      <p>
        Votre mot de passe :<br />
        <input type="password" name="password" required/>
        <% 
          if (request.getAttribute("invalid_password") != null) {
            out.print("Mot de passe incoreecte");
          } 
        %>
      </p>
      
      <p>
        <input type="submit" value="Supprimé le compte" />
      </p>
    </form>
  </body>
</html>