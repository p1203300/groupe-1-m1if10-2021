<html>
<head>
    <title>Carbone calculator - Accueil</title>
    <link rel="stylesheet" type="text/css" href="static/home.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>

</head>
<body>
<h1>Besoin d'aide ?</h1>
<div id="carbon_calculator_box">
    <br><br>

<h2>Questions fréquentes</h2>
    <h3>Qu’est-ce que l’empreinte carbone ?</h3>
    <p>L’empreinte carbone est un indicateur qui vise à mesurer l’impact d’une activité sur l’environnement, et plus particulièrement les émissions de gaz à effet de serre</p>
    <p> liées à cette activité. Elle peut s’appliquer à un individu (selon son mode de vie), à une entreprise (selon ses activités) ou un territoire.</p>
    <br>
    <h3>a quoi correspond l'unité Kg Co2-équivalents/an ?</h3>
    <p>Le Kilogramme équivalent CO2 (kg eq.CO2) est l'unité de mesure utilisé dans le cadre d'un Bilan CO2.</p>
    <p> Ce bilan mesure mesure la quantité de Gaz à Effet de Serre (GES), convertie en équivalent CO2 (dioxyde de carbone), émise lors de :</p>
    <ul>
        <li>- la production des produits (récolte, fabrication ...),</li>
        <li>- leur consommation (transports des courses vers le foyer, tri des déchets, durée d'utilisation...)</li>
        </li>
    </ul>
</div>
<br><br><br><br>


<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
