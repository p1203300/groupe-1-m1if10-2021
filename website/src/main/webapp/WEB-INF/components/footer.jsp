<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="<%=request.getContextPath()+"/static/components.css"%>">
<div class="myFooter">
    <div class="myFooterNav">
        <ul>
            <li><a href="<%=request.getContextPath()+"/index.jsp"%>">Accueil</a></li>
            <li><a href="<%=request.getContextPath()+"/about_us.jsp"%>">A propos</a></li>
            <li><a href="<%=request.getContextPath()+"/help.jsp"%>">Aide</a></li>
            <li class="p1"><a href="<%=request.getContextPath()+"/contact_us.jsp"%>">Contactez-nous</a></li>
        </ul>
        <div class="copyright">
            © 2021 CarbonCalculator - All Rights Reserved.
        </div>
    </div>
</div>