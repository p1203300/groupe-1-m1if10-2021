<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="icon" type="image/png" href="images/logo.png" />
<div id="header">
    <link rel="stylesheet" type="text/css" href="static/components.css">
    <img class="logo" id="logo_app" src="images/logo.png" alt="Logo de l'application"/>
    <div id="webcarbon_managment_header_account">
        <c:choose>
            <c:when test="${userConnected}">
                <div id="webcarbon_header_account">
                        <%--Change the current image with the user photo, if not defined -> choose the first one--%>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/SNice.svg/180px-SNice.svg.png"
                         alt="user_logo"/>
                    <form action="profil" method="post">
                        <input id="myAcount" type="button" value="Mon compte">
                    </form>
                    <a href="deconnexion">
                        <img id ="logout" src="images/logout.png" alt="Déconnexion de l'application"/>
                    </a>
                </div>
            </c:when>
            <c:otherwise>
                <form action="carbon/connexion" method="post">
                    <input id="connexion" type="submit" value="Connexion">
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</div>