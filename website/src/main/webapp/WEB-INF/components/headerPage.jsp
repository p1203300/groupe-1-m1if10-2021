<%@ page import="fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon" %>
<%@ page import="fr.univlyon1.m1if.m1if10.g1.modele.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    WebSiteCarbon wbcHeader = (WebSiteCarbon) pageContext.getServletContext().getAttribute("webSiteCarbon");
    User userHeader = null;
    if(wbcHeader != null) {
        userHeader = wbcHeader.getCurrentUser(request);
    }
    String userPhotoURL = null;
    if (userHeader != null) {
        userPhotoURL = userHeader.getPhotoURL().substring(2);
    }
%>
<link rel="icon" type="image/png" href="<%=request.getContextPath()+"/images/logo.png"%>" />
<div id="header">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()+"/static/components.css"%>" />
    <a href="<%=request.getContextPath()+"/index.jsp"%>"><img class="logo" id="logo_app" src="<%=request.getContextPath()+"/images/logo.png"%>" alt="Logo de l'application"/></a>
    <div id="webcarbon_managment_header_account">
        <c:choose>
            <c:when test="${userConnected}">
                <div id="webcarbon_header_account">
                        <%--Change the current image with the user photo, if not defined -> choose the first one--%>
                    <a href="<%=request.getContextPath()+"/carbon/deconnexion"%>">
                        <img id="logout" src="<%=request.getContextPath()+"/images/logout.png"%>" alt="Déconnexion de l'application"/>
                    </a>
                    <form action="<%=request.getContextPath()+"/carbon/profil"%>" method="post">
                        <input id="mypictureAcount" type="image" alt="monProfil"
                               src="<%=request.getContextPath()+userPhotoURL%>" value="Mon compte">
                    </form>
                </div>
            </c:when>
            <c:otherwise>
                <form action="<%=request.getContextPath()+"/carbon/connexion"%>" method="post">
                    <input id="connexion" type="submit" value="Connexion">
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</div>
