<%@ page import="fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="fr.univlyon1.m1if.m1if10.g1.modele.*" %><%--
  Created by IntelliJ IDEA.
  Date: 26/10/2021
  Time: 09:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Carbon calculator - Profil</title>
    <link rel="stylesheet" type="text/css" href="../static/profil.css">
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
    </script>
    <%@include file="WEB-INF/components/headerPage.jsp" %>
    <script type="text/javascript">
        console.debug('@script');
        <% if (request.getServletContext().getAttribute("userNotConnected") != null) { %>
        <% int loginTicketId = ((LoginTicket) request.getServletContext().getAttribute("createdLoginTicket")).getId(); %>
        let loginTicketId= <%= loginTicketId %>;
        let html = "<input type=\"hidden\" id=\"loginTicketId\" value=\"" + loginTicketId + "\" />";
        window.opener.document.body.insertAdjacentHTML('afterbegin', html);
        window.close();
        <% } %>
    </script>
    <script>
        /**
         * confirm delete account
         */
        function confirmDeleteAccount() {
            if(confirm("Êtes-vous sûr de vouloir supprimer le compte ? \n" +
                "Cela entraînera la suppression de toutes les données relatives à votre compte !")) {
                document.getElementById("deleteAccountForm").submit();
            }
            return false;
        }

        function confirmDeleteList(name, i) {
            let form = document.getElementById("deleteListForm" + i);
            form.addEventListener("submit", function(event) {
                event.preventDefault();
            });
            let message = "Êtes-vous sûr de vouloir supprimer la liste " + name + " ? \n" + "Vous ne pourrez plus la récupérer.";
            if(confirm(message)) {
                form.submit();
            }
            return false;
        }

        function confirmDeleteSimulation(name, i) {
            let form = document.getElementById("deleteSimulationForm" + i);
            form.addEventListener("submit", function(event) {
                event.preventDefault();
            });
            let message = "Êtes-vous sûr de vouloir supprimer la simulation " + name + " ? \n" + "Vous ne pourrez plus la récupérer.";
            if(confirm(message)) {
                form.submit();
            }
            return false;
        }

    </script>
    <%
        WebSiteCarbon wbc = (WebSiteCarbon) pageContext.getServletContext().getAttribute("webSiteCarbon");
        User user = wbc.getCurrentUser(request);
        float totalConstructionFootPrint = 0;
        float totalUsageFootPrint = 0;

        List<EquipmentList> userLists = wbc.getEquipmentListByUserEmail(user.getEmail());
        userLists.sort(EquipmentList.comparator);
        List<Integer> listIds = new ArrayList<Integer>();
        List<String> listNames = new ArrayList<String>();
        List<Float> constructionFootPrints = new ArrayList<Float>();
        List<Float> usageFootPrints = new ArrayList<Float>();
        List<Integer> equipmentsNumber = new ArrayList<Integer>();

        for(int i = 0; i < userLists.size(); i++) {
            List<EquipmentListElement> equipments = wbc.getEquipmentListElementsByListId(userLists.get(i).getId());
            float construction = 0;
            float usage = 0;
            for(int j = 0; j < equipments.size() ; j++) {
                Equipment equipment = equipments.get(j).getEquipement();
                float frequency = (float) (Integer.parseInt(equipments.get(j).getUsageFrequency()) * 0.5);
                Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
                Date date = equipments.get(j).getCommissionDate();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                Integer year = calendar.get(Calendar.YEAR);
                Integer quantity = equipments.get(j).getQuantity();
                construction += equipment.getConstructionFootPrint() * quantity;
                usage += equipment.getUsageFootPrint() * quantity * frequency * (actualYear - year + 1);
            }
            listIds.add(userLists.get(i).getId());
            listNames.add(userLists.get(i).getName());
            equipmentsNumber.add(equipments.size());
            constructionFootPrints.add(construction);
            usageFootPrints.add(usage);
            if(!userLists.get(i).isSimulation()) {
                totalConstructionFootPrint += construction;
                totalUsageFootPrint += usage;
            }
        }

        pageContext.getServletContext().setAttribute("totalConstructionFootPrint", totalConstructionFootPrint);
        pageContext.getServletContext().setAttribute("totalUsageFootPrint", totalUsageFootPrint);
    %>
</head>
<body>
<section>
<%--Go back here--%>

    <%
        if (request.getServletContext().getAttribute("RequestError") != null) {
            out.println("<div id=\"requestMessage\">");
            if ((boolean) request.getServletContext().getAttribute("RequestError")) {
                out.println("<div><p style=\"color:red;\"><strong>" + request.getServletContext().getAttribute("RequestMessage") + "</strong></p></div>");
            } else {
                out.println("<div><p style=\"color:green;\"><strong>" + request.getServletContext().getAttribute("RequestMessage") + "</strong></p></div>");
            }
            out.println("</div>");
            request.getServletContext().removeAttribute("RequestError");
        }

    %>
    <div id="profile_box">
        <table>
            <th id="profilTable">Profil</th>
            <tr>
                <td>
                    <table class="info">
                        <tr class="line">
                            <td class="nomUser"><%out.print(user.getFirstName() + "   " + user.getLastName()); %></td>
                        </tr>
                        <tr class="line">
                            <td><%out.print("Adresse email : " + user.getEmail()); %></td>

                        </tr>
                        <tr class="line">
                            <td>Mot de passe : ***********</td>
                        </tr>
                    </table>
                </td>
                <td class="tdGraph">
                    <div id="graphePicture">
                        <%
                            if (totalConstructionFootPrint == 0 && totalUsageFootPrint == 0) {
                                out.println("<p>Vous n'avez pas encore créé de liste!</p>");
                            } else {
                                out.println("<canvas id=\"myChart\"></canvas>");
                            }
                        %>
                        <!--<canvas id="myChart"></canvas>-->
                    </div>
                    <div id="carbon_calculator_white_box">
                        <%
                            out.println("<p>Construction : " + totalConstructionFootPrint + " ; Usage : " + totalUsageFootPrint + "</p>");
                            out.println("<p>Consommation totale : " + (totalConstructionFootPrint + totalUsageFootPrint) + " Kg Co2-équivalents/an</p>");
                        %>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <div id="modifSupp">
            <form action="editProfil" method="get">
                <input type="submit" id="editProfil" value="Modifier le profil">
            </form>
            <form action="supprimerCompte" method="post" id="deleteAccountForm">
                <input onclick="confirmDeleteAccount()" type="button" id="delete" value="Supprimer le compte">
            </form>
        </div>
        <br>
    </div>
    <div id="carbon_calculator_lists">
        <div id="ListsEquipements">
            <table>
                <th id="listes">Mes listes d'équipements</th>
                <br>
                <%
                    if (userLists.size() <= 0) {
                        out.println("<tr><td colspan=\"4\" class=\"carbon_calculator_table_header\"> Vous n'avez pas enregistré de liste.</td></tr>");
                    } else {
                        int compteur = 1;
                        for (int i = 0; i < userLists.size(); i++) {
                            if (!userLists.get(i).isSimulation()) {
                                out.print("<tr class=\"line\"><td>Profil n°" + compteur + " : " + listNames.get(i) + " (" + equipmentsNumber.get(i) + " équipement");
                                if (equipmentsNumber.get(i) > 1) out.print("s");
                                out.println(")</td>");
                                out.println("<td>Construction : " + constructionFootPrints.get(i) + "</td>");
                                out.println("<td>Utilisation : " + usageFootPrints.get(i) + "</td>");
                                out.println("<td>Total : " + (constructionFootPrints.get(i) + usageFootPrints.get(i)) + " Kg Co2 équivalents / an</td>");
                                out.println("<td><form method=\"post\" action=\"modifierListe\"><input type=\"hidden\""
                                        + "id=\"listNumber\" name=\"listNumber\" value=\"" + listIds.get(i) + "\">"
                                        + "<input class=\"carbon_input_image\" type=\"image\" name=\"submit\" src=\"../images/modifier.png\" border=\"0\" value=\"Modifier\" alt=\"Modifier\"></input></form>");

                                out.println("<td><form method=\"post\" action=\"supprimerListe\" id=\"deleteListForm" + i + "\"><input type=\"hidden\""
                                        + "id=\"listNumber\" name=\"listNumber\" value=\"" + listIds.get(i) + "\">"
                                        + "<input class=\"carbon_input_image\" type=\"image\" onclick=\"confirmDeleteList(\'" + listNames.get(i) + "\', " + i + ")\" src=\"../images/cross.png\" border=\"0\" value=\"Supprimer\" alt=\"Supprimer\"></input></form>");
                                compteur++;
                            }
                        }
                    }
                %>
            </table>
            <a href="nouvelleListe"><img class="btnAdd" src="../images/new.png"/></a>
        </div>
        <div id="ListsSimu">
            <table>
                <th id="listes">Mes simulations</th>
                <br>
                <%
                    if (userLists.size() <= 0) {
                        out.println("<tr><td colspan=\"4\" class=\"carbon_calculator_table_header\"> Vous n'avez pas enregistré de simulations.</td></tr>");
                    } else {
                        int compteur = 1;
                        for (int i = 0; i < userLists.size(); i++) {
                            if (userLists.get(i).isSimulation()) {
                                out.print("<tr class=\"line\"><td>Simulation n°" + compteur + " : " + listNames.get(i) + " (" + equipmentsNumber.get(i) + " équipement");
                                if (equipmentsNumber.get(i) > 1) out.print("s");
                                out.println(")</td>");
                                out.println("<td>Total : " + (constructionFootPrints.get(i) + usageFootPrints.get(i)) + " Kg Co2 équivalents / an</td>");
                                out.println("<td><form method=\"post\" action=\"modifierSimulation\"><input type=\"hidden\""
                                        + "id=\"listNumber\" name=\"listNumber\" value=\"" + listIds.get(i) + "\">"
                                        + "<input class=\"carbon_input_image\" type=\"image\" name=\"submit\" src=\"../images/modifier.png\" border=\"0\" value=\"Modifier\" alt=\"Modifier\"></input></form>");

                                out.println("<td><form method=\"post\" action=\"supprimerListe\" id=\"deleteSimulationForm" + i + "\"><input type=\"hidden\""
                                        + "id=\"listNumber\" name=\"listNumber\" value=\"" + listIds.get(i) + "\">"
                                        + "<input class=\"carbon_input_image\" type=\"image\" onclick=\"confirmDeleteSimulation(\'" + listNames.get(i) + "\', " + i + ")\" src=\"../images/cross.png\" border=\"0\" value=\"Supprimer\" alt=\"Supprimer\"></input></form>");
                                compteur++;
                            }
                        }
                    }
                %>
            </table>
            <a href="nouvelleSimulation"><img class="btnAdd" src="../images/new.png"/></a>
        </div>
    </div>
</section>
<br><br><br><br>
<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
<script>
    if (<%=!(totalConstructionFootPrint == 0 && totalUsageFootPrint == 0)%>) {
        var ctx = document.getElementById('myChart').getContext('2d')

        var data = {
            labels: ['Construction', 'Usage'],
            datasets: [{
                backgroundColor: ["#8089ff", "#ff8080"],
                data: [<%=totalConstructionFootPrint%>, <%=totalUsageFootPrint%>]
            }]
        }
        var options

        var config = {
            type: 'pie',
            data: data,
            options: options
        }
        var mychart = new Chart(ctx, config)
    }
</script>
