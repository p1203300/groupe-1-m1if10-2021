<html>
<head>
    <title>Carbone calculator - Accueil</title>
    <link rel="stylesheet" type="text/css" href="static/home.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>

</head>
<body>
<h1>À propos</h1>
<br>
<div id="carbon_calculator_box">
    <h2>Introduction</h2>
    <p>Carbone calculator est uen application web réalisé par 6 étudiants en master 1 dans le cadre du projet Transversal:</p>
    <ul class="name">
        <li> Lucie FOURNIER</li>
        <li> Guillaume COGONI</li>
        <li> Noauffal ABDULLATIEF</li>
        <li> Raoufdine SAID</li>
        <li> Julien GORIEU</li>
        <li> Quentin CHAPEL</li>
    </ul>

    <h2>But de l'application</h2>
    <p>Carbon calculator est une application web vous permettant de calculer les coûts annuels en CO2 de la consommation
        et de la construction
        d'une liste d'équipements que vous spécifiez.</p>
</div>
<br><br><br><br>
<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
