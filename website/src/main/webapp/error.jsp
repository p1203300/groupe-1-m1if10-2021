<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Carbone Calculator - Error</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() + "/static/error.css"%>">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<div id="errorBlock">
    <div id="errorMessageBlock">
        <table>
            <tr>
                <td><img src="/website/images/cancel.png" alt="Erreur"/></td>
                <td>
                    <c:choose>
                        <c:when test="${requestScope.get(\"msg\").contains(\"404\")}">
                            <h1>
                                Oups! Page indisponible!
                            </h1>
                            <div id="errorMessageNumber">
                                404
                            </div>
                            <div id="errorMessage">
                                Nous sommes désolés, mais la page que vous avez demandé est indisponible actuellement.
                                <br/>
                                Peut-être une erreur dans l'url que vous avez entré?
                            </div>
                        </c:when>
                        <c:when test="${requestScope.get(\"msg\").contains(\"401\")}">
                            <h1>
                                Page non accessible
                            </h1>
                            <div id="errorMessageNumber">
                                401
                            </div>
                            <div id="errorMessage">
                                Vous n'avez pas accès à cette page! Êtes-vous correctement connecté à votre compte?
                            </div>
                        </c:when>
                    </c:choose>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td id="backToMainPage">
                    <form action="<%=request.getContextPath() + "/index.jsp"%>" method="get">
                        <input class="wrapper" type="submit" value="Retourner à la page d'accueil"/>
                    </form>
                </td>
                <td id="contactUs">
                    <form action="<%=request.getContextPath() + "/contact_us.jsp"%>" method="get">
                        <input class="wrapper" type="submit" value="Nous contacter"/>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</div>
<br><br><br><br>

<%--
<%
    if (request.getAttribute("msg") != null) {
        out.print(request.getAttribute("msg"));
    }
%>
--%>

</body>
</html>