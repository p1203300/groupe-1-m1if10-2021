<html>
<head>
    <title>Carbone calculator - Accueil</title>
    <link rel="stylesheet" type="text/css" href="static/home.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>

</head>
<body>
<h1>Nous contacter</h1>
<br>
<div id="center">
    <p>Vous avez une question, un problème ? Envoyez-nous un mail, nous vous répondrons dans les plus brefs délais.</p>
    <br>
    <div class="wrapper">
        <a href="mailto:lucie.fournier2@etu.univ-lyon1.fr?subject=Demande d'informations">Nous écrire !</a>
    </div>
</div>
<br><br><br><br>

<%@include file="WEB-INF/components/footer.jsp" %>

</body>
</html>
