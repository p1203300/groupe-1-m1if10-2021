<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Carbone calculator - Accueil</title>
    <link rel="stylesheet" type="text/css" href="static/equipment.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
</head>
<body>
<%@include file="WEB-INF/components/headerPage.jsp" %>
<h1>Enregistrer mes équipements</h1>
<br>
<form action="addEquipment" method="get">
    <input type="submit" id="addEquipmentButton" value="+ Ajouter">
</form>
<div class="carbonCalculatorBox">
    <div class="equipmentProfileList">
        <div class="equipmentProfileListHeaders">
            <select name="equipmentProfileListHeadersSelect" id="equipmentProfileListHeadersSelect">
                <option value="equipmentProfile1">Profil 1</option>
            </select>
            <button id="profileEraseButton" value="x Équipement(s)"/>
        </div>
        <table class="equipementProfileListContent">
            <tr>
                <th>Équipements</th>
                <th>Fréquences</th>
                <th>Âge</th>
                <th>Empreinte carbone</th>
                <th colspan="2">Quantité</th>
            </tr>
            <tr>
                <td>PC1</td>
                <td>Freq 1</td>
                <td>x</td>
                <td>xxx tCo2/an</td>
                <td>x</td>
                <td><a href="eraseEntry"><img src="images/cross.png" alt="remove"/></a></td>
            </tr>
            <tr>
                <td>PC2</td>
                <td>Freq 2</td>
                <td>x</td>
                <td>yyy tCo2/an</td>
                <td>z</td>
                <td><a href="eraseEntry"><img src="images/cross.png" alt="remove"/></a></td>
            </tr>
            <tr>
                <td>Smartphone</td>
                <td>Freq 3</td>
                <td>z</td>
                <td>zzz tCo2/an</td>
                <td>z</td>
                <td><a href="eraseEntry"><img src="images/cross.png" alt="remove"/></a></td>
            </tr>
        </table>
    </div>
    <div class="carbonFootprintGraph">
        <img src="images/tmp/graph.png" alt="Empreinte carbone obtenu (Graphique)"/>
        <p class="tdGraph">
            xxx tCo2/an
        </p>
    </div>
</div>
<div id="equipementProfileListActionsButtons">
    <button class="equipementProfileListCancelButton">Annuler</button>

    <form action="saveProfile" method="post">
        <input type="submit" value="Enregistrer">
    </form>
</div>

<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
