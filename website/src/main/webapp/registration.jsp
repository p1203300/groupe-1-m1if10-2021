<!doctype html>
<html lang="fr">
<head>
    <title>S'inscrire</title>
    <link rel="stylesheet" type="text/css" href="../static/registration.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>

</head>
<script>
    /**
     * Change the photo url chosen by the user inside the hidden `photo_url` input
     * @param radioElement photo chosen by the user
     */
    function updatePhotoURL(radioElement) {
        let photoId = radioElement.id.replace("profile", "../images/profile/");
        let baseURL = photoId + ".png";
        let photoURLHiddenInput = document.getElementById("photo_url");
        photoURLHiddenInput.value = baseURL;
        console.log("Result : " + baseURL);
    }
</script>
<body>
<p id="result"></p>
<div id="registration_box">
    <div id="connexionForm">
        <form id="registrationForm" name="registration" action="creerCompte" method="post">
            <h2>Créer un compte</h2>
            <div class="nomPrenom">
                <div class="name">
                    <p class="asterix">*</p> Votre prénom :
                    <input type="text" placeholder="Entrer votre prénom" name="first_name" required/>
                </div>
                <div class="name">
                    <p class="asterix">*</p> Votre nom :
                    <input type="text" placeholder="Entrer votre nom" name="last_name" required/>
                </div>
            </div>
            <div class="nomPrenom">
                <div class="name">
                    <p class="asterix">*</p> Votre email :
                    <input type="text" placeholder="Entrer votre email" name="email"
                           minlength="8" maxlength="255" required/>
                </div>
                <div class="name">
                    <p class="asterix">*</p> Votre mot de passe :
                    <input type="password" placeholder="Entrer votre mot de passe" name="password"
                           minlength="8" maxlength="255" required/>
                </div>
            </div>
            <p class="incorect">
                <%
                    if (request.getAttribute("invalid_email") != null) {
                        out.print("le format de l'email est invalide");
                    }
                %>
            </p>
            <div class="dateProPhoto">
                <p>
                    <label for="birth_date">Votre date de naissance :</label>
                    <input type="date" id="birth_date" name="birth_date" min="1900-01-01">
                </p>
                <p class="lien">
                    <label for="professional">Êtes-vous un professionel ?</label>
                    <input type="checkbox" id="professional" name="professional">
                </p>
            </div>
            <input type="hidden" name="photo_url" id="photo_url" value="../images/profile/1.png"/> <br/>
            <div class="pictureSelector">
                <p>
                    Votre photo de profil :
                </p>
                <div class="cc-selector">
                    <input id="profile1" type="radio" name="credit-card"
                           value="profile1" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile1" for="profile1"></label>
                    <input id="profile2" type="radio" name="credit-card"
                           value="profile2" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile2" for="profile2"></label>
                    <input id="profile3" type="radio" name="credit-card"
                           value="profile3" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile3" for="profile3"></label>
                    <input id="profile4" type="radio" name="credit-card"
                           value="profile4" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile4" for="profile4"></label>
                    <input id="profile5" type="radio" name="credit-card"
                           value="profile5" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile5" for="profile5"></label>
                    <input id="profile6" type="radio" name="credit-card"
                           value="profile6" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile6" for="profile6"></label>
                    <input id="profile7" type="radio" name="credit-card"
                           value="profile7" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile7" for="profile7"></label>
                    <input id="profile8" type="radio" name="credit-card"
                           value="profile8" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile8" for="profile8"></label>
                    <input id="profile9" type="radio" name="credit-card"
                           value="profile9" onchange="updatePhotoURL(this)"/>
                    <label class="profile profile9" for="profile9"></label>
                </div>
            </div>
            <p class="required">les champs marqué d'une</p>
            <p class="asterix">*</p>
            <p class="required"> sont obligatoires !</p>
            <p>
                <input type="submit" value="S'inscrire"/>
            </p>
        </form>
    </div>
</div>
<br><br><br>
<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>
<script type="text/javascript" defer>
    <% if(request.getServletContext().getAttribute("userNotConnected") != null) { %>
    let html = "<input type=\"hidden\" id=\"userNotConnected\" value=\"false\" name=\"userNotConnected\" />";
    document.getElementById("registrationForm").insertAdjacentHTML('afterbegin', html);
    <% } %>
</script>