<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="../static/registration.css">
    <%@ page contentType="text/html;charset=UTF-8" %>
    <%@include file="WEB-INF/components/headerPage.jsp" %>
</head>
<script>
    <%
    WebSiteCarbon wbc = (WebSiteCarbon) pageContext.getServletContext().getAttribute("webSiteCarbon");
    User user = wbc.getCurrentUser(request);
    String[] splt = null;
    if(user.getBirthDate() != null){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = dateFormat.format(user.getBirthDate());
        splt =  date.split(" ");
    }
    %>

    /**
     * Change the photo url chosen by the user inside the hidden `photo_url` input
     * @param radioElement photo chosen by the user
     */
    function updatePhotoURL(radioElement) {
        let photoId = radioElement.id.replace("profile", "../images/profile/");
        let baseURL = photoId + ".png";
        let photoURLHiddenInput = document.getElementById("photo_url");
        photoURLHiddenInput.value = baseURL;
        console.log("Result : " + baseURL);
    }
</script>
<body>
<div id="registration_box">
    <div id="connexionForm">
        <form id="form" name="edit_profil" action="editProfil" method="post">
            <h2>Modifier les information du compte</h2>
            <div class="nomPrenom">
                <p class="name">
                    Votre prénom :<br/>
                    <input type="text" name="first_name" value="<%out.print(user.getFirstName());%>"/>
                </p>
                <p class="name">
                    Votre nom :<br/>
                    <input type="text" name="last_name" value="<%out.print(user.getLastName());%>"/>
                </p>
            </div>
            <div class="nomPrenom">
                <p class="name">
                    Votre nouveau mot de passe :<br/>
                    <input type="password" name="new_password" id="new_password"/>
                </p>
                <p class="name">
                    Retapez votre nouveau mot de passe :<br/>
                    <input type="password" id="new_password_copy"/>
                </p>
            </div>
            <div class="dateProPhoto">
                <p>
                    <label for="birth_date">Votre date de naissance :</label>
                    <input type="date" id="birth_date" name="birth_date"
                           value="<%if(user.getBirthDate() != null) {out.print(splt[0]);}%>"/>
                    <%
                        if (request.getAttribute("invalid_birth_date") != null) {
                            out.print("date de naissance invalid");
                        }
                    %>
                </p>
                <input type="hidden" name="photo_url" id="photo_url" value=<%out.print(user.getPhotoURL());%> /><br/>
                <div class="pictureSelector">
                    <p>
                        Votre photo de profil :
                    </p>
                    <div class="cc-selector">
                        <input id="profile1" type="radio" name="credit-card"
                               value="profile1" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile1" for="profile1"></label>
                        <input id="profile2" type="radio" name="credit-card"
                               value="profile2" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile2" for="profile2"></label>
                        <input id="profile3" type="radio" name="credit-card"
                               value="profile3" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile3" for="profile3"></label>
                        <input id="profile4" type="radio" name="credit-card"
                               value="profile4" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile4" for="profile4"></label>
                        <input id="profile5" type="radio" name="credit-card"
                               value="profile5" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile5" for="profile5"></label>
                        <input id="profile6" type="radio" name="credit-card"
                               value="profile6" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile6" for="profile6"></label>
                        <input id="profile7" type="radio" name="credit-card"
                               value="profile7" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile7" for="profile7"></label>
                        <input id="profile8" type="radio" name="credit-card"
                               value="profile8" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile8" for="profile8"></label>
                        <input id="profile9" type="radio" name="credit-card"
                               value="profile9" onchange="updatePhotoURL(this)"/>
                        <label class="profile profile9" for="profile9"></label>
                    </div>
                </div>
            </div>
            <div class="nomPrenom">
                <p class="name">
                    Entrez votre ancien mot de passe pour valider:<br/>
                    <input type="password" name="password" id="password" minlength="8" maxlength="255" required/>
                    <%
                        if (request.getAttribute("invalid_password") != null) {
                            out.print("Mot de passe incorrect");
                        }
                    %>
                </p>
                <p class="name">
                    <input id="modifPro" type="submit" value="editer profil"/>
                </p>
            </div>
        </form>
    </div>
</div>
<br><br><br><br>
<%@include file="WEB-INF/components/footer.jsp" %>
</body>
</html>