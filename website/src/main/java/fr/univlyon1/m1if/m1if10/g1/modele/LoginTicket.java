package fr.univlyon1.m1if.m1if10.g1.modele;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fr.univlyon1.m1if.m1if10.g1.utility.Utility;

/** Ticket de connexion.    */
@Entity
@Table(name = "login_ticket")
public class LoginTicket {

    /** Identifiant en base de donnée.  */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "login_ticket_id")
    private int id;

    /** Utilisateur propriétaire du ticket de connexion.    */
    @ManyToOne
    @JoinColumn(name = "user_email")
    private User user;

    /** Date de début de validité du ticket de connexion.   */
    @Column(name = "begin_date")
    private LocalDateTime beginDate;

    /** Date de fin de validité du ticket de connexion. */
    @Column(name = "end_date")
    private LocalDateTime endDate;

    /** Clé.    */
    @Column(name = "key")
    private String key;

    /** Constructeur par défaut.    */
    public LoginTicket() {
    }

    /**
     * @param pUser Utilisateur propriétaire.
     * @param pBeginDate Date de début de validité.
     * @param pEndDate  Date de fin de validité.
     *
     * @brief Constructeur: Prend en paramètre l'utilisateur propriétaire du
     *          ticket de connexion, la date de début et de fin de validité du
     *          ticket de connexion.
     * @throws NoSuchAlgorithmException
     */
    public LoginTicket(final User pUser, final LocalDateTime pBeginDate,
                       final LocalDateTime pEndDate)
            throws NoSuchAlgorithmException {
        this.user = pUser;
        this.beginDate = pBeginDate;
        this.endDate = pEndDate;
        String str = user.getEmail() + user.getPasswordHashed()
                            + pBeginDate.toString() + pEndDate.toString();
        key = Utility.sha256(str);
    }

    /**
     * Renvoie l'identifiant en base de donnée du ticket de connexion.
     *
     * @return l'identifiant en base de donnée du ticket de connexion.
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie l'utilisateur propriétaire du ticket de connexion.
     *
     * @return l'utilisateur propriétaire du ticket de connexion.
     */
    public User getUser() {
        return user;
    }

    /**
     * Renvoie la date de début de validité du ticket de connexion.
     *
     * @return la date de début de validité du ticket de connexion.
     */
    public LocalDateTime getBeginDate() {
        return beginDate;
    }

    /**
     * Renvoie la date de fin de validité du ticket de connexion.
     *
     * @return la date de fin de validité du ticket de connexion.
     */
    public LocalDateTime getEndDate() {
        return endDate;
    }

    /**
     * Renvoie la clé du ticket de connexion.
     *
     * @return la clé du ticket de connexion.
     */
    public String getKey() {
        return key;
    }

    /**
     * Renvoie true si le ticket de connexion a expiré.
     *
     * @return true si le ticket de connexion a expiré.
     */
    public boolean isExpirated() {
        return Utility.convertLocalDateTimeToMills(endDate)
                < Utility.convertLocalDateTimeToMills(LocalDateTime.now());
    }

    /**
     * @param u Nouvelle utilisateur propriétaire du ticket de connexion.
     *
     * @brief   Changer l'utilisateur propriétaire du ticket de connexion.
     */
    public void setUser(final User u) {
        this.user = u;
    }

    /**
     * @param pBeginDate    Nouvelle date de début de validité;
     *
     * @brief Changer la date de début de validité du ticket de connexion.
     */
    public void setBeginDate(final LocalDateTime pBeginDate) {
        this.beginDate = pBeginDate;
    }

    /**
     * @param endDateV  Nouvelle date de fin de validité.
     *
     * @brief Changer la date de fin de validité du ticket de connexion.
     */
    public void setEndDate(final LocalDateTime endDateV) {
        this.endDate = endDateV;
    }
}
