package fr.univlyon1.m1if.m1if10.g1.DAO;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import fr.univlyon1.m1if.m1if10.g1.modele.Equipment;

public class EquipmentDAO {

    /**
     * Entity Manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public EquipmentDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }


    /**
     * retourne un equipement en fonction du nom et de la categorie.
     * @param categoryTitle
     * @param equipmentName
     * @return Equipment.
     */
    public Equipment getEquipmentByNameAndCategoryTitle(
        final String categoryTitle,
        final String equipmentName) {
        TypedQuery<Equipment> q = em.createQuery(
                "SELECT equip FROM Equipment equip WHERE equip.category.title = ?1 AND equip.name = ?2", Equipment.class);
        q.setParameter(1, categoryTitle);
        q.setParameter(2, equipmentName);
        Collection<Equipment> results = q.getResultList();
        if (!results.isEmpty()) {
            return results.iterator().next();
        }
        return null;
    }

    /**
     * Recupere une liste d'equipement.
     * @param categoryTitle
     * @return une liste d'equipement.
     */
    public List<Equipment> getEquipmentsByCategory(final String categoryTitle) {
        TypedQuery<Equipment> q = em.createQuery(
                "SELECT equip FROM Equipment equip WHERE equip.category.title = ?1",
                   Equipment.class);
        q.setParameter(1, categoryTitle);
        return q.getResultList();
    }
}
