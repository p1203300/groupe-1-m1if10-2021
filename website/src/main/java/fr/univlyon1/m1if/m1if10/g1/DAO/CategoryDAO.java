package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.Category;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class CategoryDAO {

    /**
     * Entity manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public CategoryDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     * Récupere une Liste de category.
     * @return List de Category.
     */
    public List<Category> getCategoryList() {
        TypedQuery<Category> q = em.createQuery("SELECT cat FROM Category cat",
        Category.class);
        return q.getResultList();
    }

    /**
     * Recupere la category 'nom' dans la bd.
     * @param nom
     * @return Category.
     */
    public Category getCategoryByNom(final String nom) {
        return em.find(Category.class, nom);
    }
}

