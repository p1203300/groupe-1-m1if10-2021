package fr.univlyon1.m1if.m1if10.g1.modele;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import java.io.Serializable;

/**
 * Représente un équipement et ses informations.
 */
@Entity
@Table(name = "equipment")
public class Equipment  implements Serializable {

    /** La catégorie de l'équipement.   */
    @Id
    @ManyToOne
    @JoinColumn(name = "category_title")
    private Category category;

    /** Nom de l'équipement.    */
    @Id
    @Column(name = "equipment_name")
    private String name;

    /** Durée de vie moyenne de l'équipement.   */
    @Column(name = "average_life_duration")
    private float averageLifeDuration;

    /**
     * Empreinte carbone de la construction de l'équipement
     * (Kg CO2 équivalents).
     */
    @Column(name = "construction_footprint")
    private float constructionFootPrint;

    /**
     * Empreinte carbone annuelle de l'utilisation de l'équipement
     * ( Kg CO2 équivalents / an).
     */
    @Column(name = "usage_footprint")
    private float usageFootPrint;

    /**
     * Renvoie la catégorie de l'équipement.
     *
     * @return catégorie de l'équipement.
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Renvoie le nom de l'équipement.
     *
     * @return nom de l'équipement.
     */
    public String getName() {
        return name;
    }

    /**
     * Renvoie la durée de vie moyenne de l'équipement.
     *
     * @return durée de vie moyenne de l'équipement.
     */
    public float getAverageLifeDuration() {
        return averageLifeDuration;
    }

    /**
     * Renvoie l'empreinte carbone de la construction de l'équipement
     * (Kg CO2 équivalents).
     *
     * @return empreinte carbone de la construction de l'équipement
     *          (Kg CO2 équivalents).
     */
    public float getConstructionFootPrint() {
        return constructionFootPrint;
    }

    /**
     * Renvoie l'empreinte carbone annuelle de l'utilisation de l'équipement
     * (Kg CO2 équivalents / an).
     *
     * @return empreinte carbone annuelle de l'utilisation de l'équipement
     * (Kg CO2 équivalents / an).
     */
    public float getUsageFootPrint() {
        return usageFootPrint;
    }

    /**
     * @param c Nouvelle catégorie.
     *
     * @brief Changer la catégorie de l'équipement.
     */
    public void setCategory(final Category c) {
        this.category = c;
    }

    /**
     * @param n Nouveau nom de l'équipement.
     *
     * @brief Changer le nom de l'équipement.
     */
    public void setName(final String n) {
        this.name = n;
    }

    /**
     * @param ald Nouvelle durée de vie moyenne de l'équipement.
     *
     * @brief Changer la durée de vie moyenne de l'équipement.
     */
    public void setAverageLifeDuration(final float ald) {
        this.averageLifeDuration = ald;
    }

    /**
     * @param construcFootPrint Nouvelle empreinte carbone de la construction.
     *
     * @brief Changer l'empreinte carbone de la construction de l'équipement
     *        (Kg CO2 équivalents).
     */
    public void setConstructionFootPrint(final float construcFootPrint) {
        this.constructionFootPrint = construcFootPrint;
    }

    /**
     * @param u Nouvelle empreinte carbone annuelle.
     *
     * @brief Changer l'empreinte carbone annuelle de l'utilisation de
     *        l'équipement (Kg CO2 équivalents / an).
     */
    public void setUsageFootPrint(final float u) {
        this.usageFootPrint = u;
    }
}
