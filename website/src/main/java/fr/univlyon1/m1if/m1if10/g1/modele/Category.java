package fr.univlyon1.m1if.m1if10.g1.modele;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui représente une catégorie d'équipement.
 */
@Entity
@Table(name = "category")
public class Category {

    /** Titre de la catégorie.  */
    @Id
    @Column(name = "category_title")
    private String title;

    /** Description de la catégorie.    */
    @Column(name = "description")
    private String description;

    /** Liste des équipements de cette catégorie.   */
    @OneToMany(mappedBy = "category")
    private List<Equipment> equipments = new ArrayList<>();

    /** Constructeur par défaut: Me le titre et la description à null.  */
    public Category() {
        this.title = null;
        this.description = null;
    }

    /**
     * Renvoie le titre de la catégorie.
     *
     * @return le titre de la catégorie.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Renvoie la description de la catégorie.
     *
     * @return la description de la catégorie.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param t Nouveau titre de la catégorie.
     *
     * @brief   Change le titre de la catégorie.
     */
    public void setTitle(final String t) {
        this.title = t;
    }

    /**
     * @param d Nouvelle description de la catégorie.
     *
     * @brief Change le description de la catégorie.
     */
    public void setDescription(final String d) {
        this.description = d;
    }
}
