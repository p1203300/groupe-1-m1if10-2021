package fr.univlyon1.m1if.m1if10.g1.modele;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/** Un utilisateur. */
@Entity
@Table(name = "users")
public class User {
    /** Email de l'utilisateur. */
    @Id
    @Column(name = "user_email")
    private String email;

    /** hash du mot de passe de l'utilisateur.    */
    @Column(name = "password_hashed")
    private String passwordHashed;

    /** À true l'utilisateur est un professionnel.   */
    @Column(name = "professional")
    private boolean isProfessional;

    /** Nom de famille.  */
    @Column(name = "last_name")
    private String lastName;

    /** Prénom.  */
    @Column(name = "first_name")
    private String firstName;

    /** Date de naissance.  */
    @Column(name = "birth_date")
    private Date birthDate;

    /** Url de la photo. */
    @Column(name = "photo_url")
    private String photoURL;

    /** Liste des tickets de connexion de l'utilisateur.    */
    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LoginTicket> loginTicketList = new ArrayList<>();

    /** Liste des listes d'équipement et des simulation de l'utilisateur.   */
    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EquipmentList> equipmentLists = new ArrayList<>();

    /** Constructeur par défaut.    */
    public User() {
    }

    /**
     * @param pIsProfessional   L'utilisateur est un professionel
     * @param pEmail    L'email de l'utilsateur
     * @param pPasswordHashed   Le hash du mot de passe
     *
     * Constructeur: Prend en paramètre un booléen indiquant que l'utilisateur
     *  est un professionel, l'email de l'utilisateur et le hash du mot de
     *  passe.
     */
    public User(final boolean pIsProfessional, final String pEmail,
                final String pPasswordHashed) {
        this.isProfessional = pIsProfessional;
        this.email = pEmail;
        this.passwordHashed = pPasswordHashed;
    }

    /**
     * Renvoie l'email de l'utilisateur.
     *
     * @return l'email de l'utilisateur.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Renvoie true si l'utilisateur est un professionel.
     *
     * @return true si l'utilisateur est un professionel.
     */
    public boolean isProfessional() {
        return isProfessional;
    }

    /**
     * Renvoie le hash du mot de passe de l'utilisateur.
     *
     * @return le hash du mot de passe de l'utilisateur.
     */
    public String getPasswordHashed() {
        return passwordHashed;
    }

    /**
     * Renvoie le nom de famille de l'utilisateur.
     *
     * @return le nom de famille de l'utilisateur.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Renvoie le prénom de l'utilisateur.
     *
     * @return le prénom de l'utilisateur.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Renvoie la date de naissance de l'utilisateur.
     *
     * @return la date de naissance de l'utilisateur.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Renvoie l'URL de la photo de l'utilisateur.
     *
     * @return l'URL de la photo de l'utilisateur.
     */
    public String getPhotoURL() {
        return photoURL;
    }

    /**
     * @param passwordHash  Nouveau hash du mot de passe.
     *
     * @brief Changer le hash du mot de passe.
     */
    public void setPasswordHashed(final String passwordHash) {
        this.passwordHashed = passwordHash;
    }

    /**
     * @param lastNam Nouveau nom de famille.
     *
     * @brief Changer le nom de famille.
     */
    public void setLastName(final String lastNam) {
        this.lastName = lastNam;
    }

    /**
     * @param firstNam Nouveau prénom.
     *
     * @brief Changer le prénom.
     */
    public void setFirstName(final String firstNam) {
        this.firstName = firstNam;
    }

    /**
     * @param birthDat Nouvelle date de naissance.
     * @brief Changer la date de naissance.
     */
    public void setBirthDate(final Date birthDat) {
        this.birthDate = birthDat;
    }

    /**
     * @param url Nouvelle url de la photo.
     * @brief Changer l'url de la photo.
     */
    public void setPhotoURL(final String url) {
        this.photoURL = url;
    }

    public void setProfessional(boolean isProfessional) {
        this.isProfessional = isProfessional;
    }
}
