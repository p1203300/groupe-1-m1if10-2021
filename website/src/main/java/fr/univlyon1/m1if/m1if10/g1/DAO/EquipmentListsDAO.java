package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentList;
import fr.univlyon1.m1if.m1if10.g1.modele.User;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class EquipmentListsDAO {

    /**
     * Entity manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public EquipmentListsDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     * Creer une EquipmentList et enregistre en bd.
     * @param user
     * @param isSimulation
     * @param name
     * @return EquimpentList.
     */
    public EquipmentList createEquipmentList(final User user,
                                             final boolean isSimulation,
                                             final String name) {
        EquipmentList equipmentList
            = new EquipmentList(user, isSimulation, name);
        em.getTransaction().begin();
        em.persist(equipmentList);
        em.getTransaction().commit();
        return equipmentList;
    }

    /**
     * recupere ListEquipmentList en fonction de l'user email.
     * @param userEmail
     * @return Liste d'EquipmentList.
     */
    public List<EquipmentList> getEquipmentListByUserEmail(
                                final String userEmail) {
        TypedQuery<EquipmentList> q = em.createQuery(
        "SELECT list FROM EquipmentList list WHERE list.user.email = ?1",
         EquipmentList.class);
        q.setParameter(1, userEmail);
        return q.getResultList();
    }

    /**
     * @param userEmail
     * @return Liste d'EquipmentList.
     */
    public List<EquipmentList> getEquipmentListNoSimulationByUserEmail(
                                              final String userEmail) {
        TypedQuery<EquipmentList> q = em.createQuery(
                "SELECT list FROM EquipmentList list WHERE list.user.email = ?1 AND list.isSimulation = false",
                 EquipmentList.class);
        q.setParameter(1, userEmail);
        return q.getResultList();
    }

    /**
     * recupere une EquipmentList en fonction de l'id.
     * @param id
     * @return EquipmentList.
     */
    public EquipmentList getEquipmentListByListID(final int id) {
        return em.find(EquipmentList.class, id);
    }

    /**
     * Supprime une liste d'equipement.
     * @param list
     */
    public void deleteEquipmentList(final EquipmentList list) {
        em.getTransaction().begin();
        em.remove(list);
        em.getTransaction().commit();
        em.clear();
    }

    /**
     *  supprime tous les liste d'équipement.
     * @param user
     */
    public void deleteAllEquipmentList(final User user) {
        List<EquipmentList> listEquipmentList
            = getEquipmentListByUserEmail(user.getEmail());
        for (EquipmentList el : listEquipmentList) {
            em.getTransaction().begin();
            Query q = em.createQuery("DELETE FROM EquipmentListElement e WHERE e.equipmentList.id = ?1");
            q.setParameter(1, el.getId());
            q.executeUpdate();
            em.remove(el);
            em.getTransaction().commit();
            em.clear();
        }
    }
}

