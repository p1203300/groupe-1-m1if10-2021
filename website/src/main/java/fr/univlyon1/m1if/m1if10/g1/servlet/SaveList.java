package fr.univlyon1.m1if.m1if10.g1.servlet;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.Equipment;
import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentList;
import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentListElement;
import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "SaveList", value = "/SaveList")
public class SaveList extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean pass = true;
        String requestError = "RequestError";
        String requestMessage = "RequestMessage";
        request.getServletContext().setAttribute(requestError, false);
        Integer listID = -1;
        Boolean isSimulation = false;
        final Logger logger = LoggerFactory.getLogger(SaveList.class);

        try {
            response.setContentType("text/html; charset=UTF-8");
            request.setCharacterEncoding("UTF-8");
            WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
            if(request.getParameter("createdLoginTicket") != null) {
                int loginTicketId = Integer.parseInt(request.getParameter("createdLoginTicket"));
                LoginTicket loginTicket = wbc.getLoginTicketById(loginTicketId);
                if(loginTicket == null) {
                    request.getServletContext().setAttribute(requestError, true);
                    request.getServletContext().setAttribute(requestMessage, "Votre liste d'équipements n'a pas pu être enregistrée. Merci de réessayer.");
                    response.sendRedirect("../index.jsp");
                    return;
                }
                Cookie cookieId = new Cookie("loginTicketId",""+ loginTicket.getId());
                cookieId.setHttpOnly(true);
                cookieId.setSecure(true);
                Cookie cookieKey = new Cookie("loginTicketKey", loginTicket.getKey());
                cookieKey.setHttpOnly(true);
                cookieKey.setSecure(true);

                response.addCookie(cookieId);
                response.addCookie(cookieKey);
            }
            listID = Integer.parseInt(request.getParameter("listNumber"));
            int simulation = Integer.parseInt(request.getParameter("isSimulation"));
            isSimulation = (simulation == 1);
            if(listID < 0 && Boolean.TRUE.equals(!isSimulation)) {
                request.getServletContext().setAttribute(requestMessage, "Votre liste a bien été enregistrée.");
            }
            else if (listID > 0 && Boolean.TRUE.equals(!isSimulation)) {
                request.getServletContext().setAttribute(requestMessage, "Votre liste a bien été modifiée.");
            }
            else if(listID < 0 && Boolean.TRUE.equals(!isSimulation)) {
                request.getServletContext().setAttribute(requestMessage, "Votre simulation a bien été enregistrée.");
            }
            else {
                request.getServletContext().setAttribute(requestMessage, "Votre simulation a bien été modifiée.");
            }
            Integer number = Integer.parseInt(request.getParameter("equipmentsNumber"));
            if(number > 0 && wbc != null) {
                EquipmentList list = wbc.createEquipmentList(wbc.getCurrentUser(request), isSimulation, request.getParameter("listName"));
                if(list != null) {
                    for (int i = 1; i <= number; i++) {
                        String equipmentName = request.getParameter("equipmentName" + i);
                        String equipmentCategory = request.getParameter("categoryTitle" + i);
                        if(equipmentName != null && !equipmentName.isEmpty() && equipmentCategory != null && !equipmentCategory.isEmpty()) {
                            Equipment equipment = wbc.getEquipmentByNameAndCategoryTitle(equipmentCategory, equipmentName);
                            if (equipment != null) {
                                Integer year = Integer.parseInt(request.getParameter("year" + i));
                                Date date = new GregorianCalendar(year, 1, 1).getTime();
                                Integer frequency = Integer.parseInt(request.getParameter("frequency" + i));
                                Integer quantity = Integer.parseInt(request.getParameter("quantity" + i));
                                Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
                                if(year <= actualYear && year >= 1980 && (frequency == 1 || frequency == 2 || frequency == 4) && quantity > 0) {
                                    EquipmentListElement element = wbc.createEquipmentListElement(list, equipment, date, frequency.toString(), quantity);
                                    if(element == null) {
                                        pass = false;
                                    }
                                }
                                else {
                                    pass = false;
                                }
                            }
                            else {
                                pass = false;
                            }
                        }
                        else {
                            pass = false;
                        }
                    }
                }
                else {
                    pass = false;
                }
                if(!pass) {
                    wbc.deleteEquipmentList(list);
                }
            }
            else {
                pass = false;
            }
        }
        catch (Exception e) {
            request.getServletContext().setAttribute(requestError, true);
            request.getServletContext().setAttribute(requestMessage, "Votre liste d'équipements n'a pas pu être enregistrée. Merci de réessayer.");
        }

        if(!pass) {
            request.getServletContext().setAttribute(requestError, true);
            request.getServletContext().setAttribute(requestMessage, "Les données saisies étaient erronnées. Votre liste d'équipements n'a pas été enregistrée.");
        }
        else if(listID > -1){
            try {
                WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
                EquipmentList list = wbc.getEquipmentListByListID(listID);
                wbc.deleteEquipmentList(list);
            }
            catch (Exception e) {
                request.getServletContext().setAttribute(requestError, true);
                request.getServletContext().setAttribute("RequestMessage", "Votre liste a été dupliquée par erreur. Veuillez réessayer.");
            }
        }
        try {
            response.sendRedirect("profil");
        }catch(IOException e) {
            logger.error("Exception Context : ", e);
        }
    }
}
