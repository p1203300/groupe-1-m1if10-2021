package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "EditProfil", value = "/EditProfil")
public class EditProfil extends HttpServlet {

    final Logger logger = LoggerFactory.getLogger(EditProfil.class);
    static final String EXCEPTION_TXT = "Exception happened !";

    boolean checkLength(String lastName, String firstName, String photoURL) {
        return ((lastName != null && lastName.length() > 255)
                || (firstName != null && firstName.length() > 255)
                || (photoURL != null && photoURL.length() > 4096));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
        boolean isProfessional = request.getParameter("professional") != null;
        String password = request.getParameter("password");
        String newPassword = request.getParameter("new_password");
        String lastName = request.getParameter("last_name");
        String firstName = request.getParameter("first_name");
        String photoURL = request.getParameter("photo_url");
        Date birthDate = null;
        String birthDateStr = request.getParameter("birth_date");
        System.out.println(password);
        System.out.println(newPassword);
        try {
            if (checkLength(lastName, firstName, photoURL)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            if (password == null || password.equals("")) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            if (!Utility.isValidPassword(password)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        } catch (IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }

        if (birthDateStr != null && !birthDateStr.equals("")) {
            try {
                birthDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(birthDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (birthDate != null && !Utility.isValidBirthDate(birthDate)) {
            request.setAttribute("invalid_birth_date", true);
            try {
                request.getRequestDispatcher("edit_profil.jsp").include(request, response);
            } catch (ServletException | IOException e) {
                logger.error(EXCEPTION_TXT, e);
            }
            return;
        }


        User user = wbc.getCurrentUser(request);
        try {
            if (!Utility.sha256(password).equals(user.getPasswordHashed())) {
                request.setAttribute("invalid_password", true);
                request.getRequestDispatcher("edit_profil.jsp").include(request, response);

                return;
            }
        } catch (NoSuchAlgorithmException | ServletException e) {
            logger.error(EXCEPTION_TXT, e);
        }

        if (newPassword != null && !newPassword.equals("") && !Utility.isValidPassword(newPassword)) {
            try {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IOException e) {
                logger.error(EXCEPTION_TXT, e);
            }
            return;
        }

        if (newPassword != null && !newPassword.equals("")) {
            password = newPassword;
        }

        try {
            wbc.updateUser(user.getEmail(), isProfessional, Utility.sha256(password), lastName, firstName, birthDate, photoURL);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            response.sendRedirect("profil");
        } catch (IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }
    }
}
