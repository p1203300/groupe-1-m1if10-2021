package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.Equipment;
import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentList;
import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentListElement;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class EquipmentListElementDAO {
    /**
     * Entity Manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public EquipmentListElementDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     * creer une EquipmentListElement et l'enrgistre dans la bd.
     * @param list
     * @param equipment
     * @param date
     * @param frequency
     * @param quantity
     * @return EquipmentListElement.
     */
    public EquipmentListElement createEquipmentListElement(
        final EquipmentList list, final Equipment equipment,
        final Date date, final String frequency, final Integer quantity) {
        EquipmentListElement equipmentListElement = new EquipmentListElement(
                                 list, equipment, date, frequency, quantity);
        em.getTransaction().begin();
        em.persist(equipmentListElement);
        em.getTransaction().commit();
        return equipmentListElement;
    }

    /**
     * Recupere une EquipmentListElement
     * en fonction d'une List.
     * @param listId
     * @return List<EquipmentListElement>.
     */
    public List<EquipmentListElement> getEquipmentListElementsByListId(
                                                    final int listId) {
        TypedQuery<EquipmentListElement> q = em.createQuery(
                "SELECT elem FROM EquipmentListElement elem WHERE elem.equipmentList.id = ?1 ",
                 EquipmentListElement.class);
        q.setParameter(1, listId);
        return q.getResultList();
    }

    /**
     * Recupere une liste d'équipement en fonction d'une liste.
     * @param listId
     * @return une liste d'Equipment.
     */
    public List<Equipment> getEquipmentsByListId(final int listId) {
        TypedQuery<Equipment> q = em.createQuery(
                "SELECT elem.equipment FROM EquipmentListElement elem WHERE elem.equipmentList.id = ?1 ", Equipment.class);
        q.setParameter(1, listId);
        return q.getResultList();
    }

    /**
     * Recupere une EquipmentListElement
     * en fonction d'une List et d'un Equipment.
     * @param listId
     * @param equipment
     * @return une Liste d'EquipementListElement.
     */
    public List<EquipmentListElement>
        getEquipmentListElementByListAndEquipment(final int listId,
                                                  final Equipment equipment) {
        TypedQuery<EquipmentListElement> q = em.createQuery(
                "SELECT elem FROM EquipmentListElement elem WHERE elem.equipmentList.id = ?1 AND elem.equipment.category = ?2 AND elem.equipment.name = ?3 ", EquipmentListElement.class);
        q.setParameter(1, listId);
        q.setParameter(2, equipment.getCategory().getTitle());
        q.setParameter(3, equipment.getName());
        return q.getResultList();
    }
}
