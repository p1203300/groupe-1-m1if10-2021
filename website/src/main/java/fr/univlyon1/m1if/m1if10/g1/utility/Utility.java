package fr.univlyon1.m1if.m1if10.g1.utility;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.EmailValidator;

public final class Utility {
    /**
     * Minimum length.
     */
    public static final int MIN_LENGTH = 8;

    /**
     * Maximum length.
     */
    public static final int MAX_LENGTH = 255;

    /**
     * Maximum age.
     */
    public static final int MAX_AGE = 150;

    /**
     * 255 en hexadecimal.
     */
    public static final int BYTE_255 = 0xff;

    /**
     * 256 en hexadecimal.
     */
    public static final int BYTE_256  = 0x100;

    /**
     * RADIX (method toString).
     */
    public static final int RADIX  = 16;

    /** Constructeur par défaut.    */
    private Utility() {
        throw new IllegalStateException(
                "Utility class must not be instantiated");
    }

    /**
     * Verifie si le mot de passe est valide.
     * @param password
     * @return vrai si le mot de passe est correct faux sinon.
     */
    public static boolean isValidPassword(final String password) {
        return password.length() >= MIN_LENGTH
                && password.length() < MAX_LENGTH;
    }

    /**
     * Verifie si l'email est valide.
     * @param email
     * @return vrai si l'email est correct faux sinon.
     */
    public static boolean isValidEmail(final String email) {
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }


    /**
     * Verifie si la date de naissance est valide.
     * @param birthDate
     * @return vrai si la date est correct faux sinon.
     */
    public static boolean isValidBirthDate(final Date birthDate) {
        Date today = new Date();
        int age = calculateAge(convertToLocalDateViaInstant(birthDate),
                convertToLocalDateViaInstant(today));
        return (age > 1 && age < MAX_AGE);
    }

    /**
     * Convertit une Date en localDate.
     * @param dateToConvert
     * @return un LocalDate
     */
    public static LocalDate convertToLocalDateViaInstant(
            final Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    /**
     * Calcule l'age.
     * @param birthDate
     * @param currentDate
     * @return int qui represente l'age.
     */
    public static int calculateAge(final LocalDate birthDate,
                                   final LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    /**
     * converti une date en ms, vers une date en LocalDateTime.
     * @param millis
     * @return un LocalDateTime.
     */
    public static LocalDateTime convertMillsToLocalDateTime(final long millis) {
        Instant instant = Instant.ofEpochMilli(millis);
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * convertit le type LocalDateTime en Milliseconds.
     * @param ldt
     * @return un long (ms).
     */
    public static long convertLocalDateTimeToMills(final LocalDateTime ldt) {
        Instant instant = ldt.atZone(ZoneId.systemDefault()).toInstant();
        return instant.toEpochMilli();
    }

    /**
     * Permet de hacher une chaine de caractere.
     * @param s
     * @return une chaine de caractere haché.
     * @throws NoSuchAlgorithmException
     */
    public static String sha256(final String s)
            throws NoSuchAlgorithmException {
        MessageDigest msg = MessageDigest.getInstance("SHA-256");
        byte[] hash = msg.digest(s.getBytes(StandardCharsets.UTF_8));
        // convertir bytes en hexadécimal
        StringBuilder strBuilder = new StringBuilder();
        for (byte b : hash) {
            strBuilder.append(Integer.toString((b & BYTE_255)
                    + BYTE_256, RADIX).substring(1));
        }
        return strBuilder.toString();
    }

    /**
     * Récupere le cookie courrant.
     * @param request
     * @param cookieName
     * @return cookie courrany.
     */
    public static Cookie getCookie(final HttpServletRequest request,
                                   final String cookieName) {
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * Supprime le cookie courrant.
     * @param request
     * @param response
     * @param cookieName
     */
    public static void deleteCookie(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final String cookieName) {
        Cookie cookie = getCookie(request, cookieName);
        if (cookie != null) {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            response.addCookie(cookie);
        }
    }
}