package fr.univlyon1.m1if.m1if10.g1.modele;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** Une liste d'équipement. */
@Entity
@Table(name = "equipments_list")
public class EquipmentList {

    /** Identifiant d'une liste d'équipement.   */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "equipments_list_id")
    private int id;

    /** Propriétaire d'une liste d'équipement.  */
    @ManyToOne
    @JoinColumn(name = "user_email")
    private User user;

    /** La liste d'équipement est une simulation ou pas.    */
    @Column(name = "simulation")
    private boolean isSimulation;

    /** Nom de la liste d'équipement.   */
    @Column(name = "name")
    private String name;

    /** Date/Heure de création.    */
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    /** Date/Heure de la dernière modification. */
    @Column(name = "last_modification_date")
    private LocalDateTime lastModificationDate;

    /** Liste des élements de la liste d'équipements.   */
    @OneToMany(mappedBy = "equipmentList",
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EquipmentListElement> equipments = new ArrayList<>();

    /** Constructeur par défaut: Ne fait rien.    */
    public EquipmentList() {
    }

    /**
     * @param u Utilisateur propriétaire de la liste d'équipement.
     * @param isSimul La liste d'équipement est une simulation.
     * @param n Nom de la liste d'équipement.
     *
     * @brief  Constructeur: Initialise la nouvelle liste avec u comme
     *         propriétaire, si isSimul est à true le définit comme
     *         simulation et n le nom de la liste.
     */
    public EquipmentList(final User u, final boolean isSimul, final String n) {
        this.user = u;
        this.isSimulation = isSimul;
        this.name = n;
        creationDate = LocalDateTime.now();
        lastModificationDate = LocalDateTime.now();
    }

    /**
     * Renvoie l'identifiant de la liste d'équipement.
     *
     * @return identifiant de la liste d'équipement.
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie true si la liste est une simulation.
     *
     * @return true si la liste est une simulation.
     */
    public boolean isSimulation() {
        return isSimulation;
    }

    /**
     * Renvoie l'utilisateur propriétaire de la liste.
     *
     * @return l'utilisateur propriétaire de la liste.
     */
    public User getUser() {
        return user;
    }

    /**
     * Renvoie le nom de la liste.
     *
     * @return le nom de la liste.
     */
    public String getName() {
        return name;
    }

    /**
     * Renvoie la date de création de la liste d'équipement.
     *
     * @return la date de création de la liste d'équipement.
     */
    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    /**
     * Renvoie la date de dernière modification.
     *
     * @return la date de dernière modification.
     */
    public LocalDateTime getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * Renvoie la liste des éléments de la liste d'équipement.
     *
     * @return la liste des éléments de la liste d'équipement.
     */
    public List<EquipmentListElement> getEquipments() {
        return equipments;
    }

    /**
     * @param n Nouveau nom.
     *
     * @brief Changer le nom de la liste d'équipement.
     */
    public void setName(final String n) {
        this.name = n;
    }

    /**
     * @param lastModifDate Nouvelle date de dernière modification.
     *
     * @brief Changer la date de dernière modification.
     */
    public void setLastModificationDate(final LocalDateTime lastModifDate) {
        this.lastModificationDate = lastModifDate;
    }

    public static Comparator<EquipmentList> comparator = new Comparator<EquipmentList>() {
        public int compare(EquipmentList l1, EquipmentList l2)
        {
            return l1.getName().compareTo(l2.getName());
        }
    };
}
