package fr.univlyon1.m1if.m1if10.g1.servlet;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;

@WebServlet(name = "Init", value = "/Init", loadOnStartup = 0)
public class Init extends HttpServlet {

    private EntityManager em;
    private WebSiteCarbon wbc;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        if (getServletContext().getAttribute("webSiteCarbon") == null) {
            em = Persistence.createEntityManagerFactory("com.mycompany_mavenproject1_war_1.0-SNAPSHOTPU").createEntityManager();
            wbc = new WebSiteCarbon(em); // mettre dans le context des servlets
            getServletContext().setAttribute("webSiteCarbon", wbc);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        em.close();
    }
}
