package fr.univlyon1.m1if.m1if10.g1.metier;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Cookie;

import fr.univlyon1.m1if.m1if10.g1.DAO.*;
import fr.univlyon1.m1if.m1if10.g1.modele.*;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;

public class WebSiteCarbon {
    private CategoryDAO catDao;
    private EquipmentDAO equipDao;
    private EquipmentListsDAO equipListDao;
    private EquipmentListElementDAO equipListElemDao;
    private UserDAO userDao;
    private LoginTicketDAO loginTicketDAO;

    public WebSiteCarbon(EntityManager em){
        this.catDao = new CategoryDAO(em);
        this.equipDao = new EquipmentDAO(em);
        this.equipListDao = new EquipmentListsDAO(em);
        this.equipListElemDao = new EquipmentListElementDAO(em);
        this.userDao = new UserDAO(em);
        this.loginTicketDAO = new LoginTicketDAO(em);
    }

    public Category getCategory(String title){
        return catDao.getCategoryByNom(title);
    }

    public List<Category> getCategoryList(){
        return catDao.getCategoryList();
    }

    public Category getCategoryByNom(String title){
        return catDao.getCategoryByNom(title);
    }
    //// EQUIPMENTS /////

    public Equipment getEquipmentByNameAndCategoryTitle(String categoryTitle, String equipmentName){
        return equipDao.getEquipmentByNameAndCategoryTitle(categoryTitle, equipmentName);
    }
    public List<Equipment> getEquipmentsByCategory(String category_title){
        return equipDao.getEquipmentsByCategory(category_title);
    }

    /// EQUIPMENTLIST ////

    public List<EquipmentList> getEquipmentListByUserEmail(String userEmail){
        return equipListDao.getEquipmentListByUserEmail(userEmail);
    }

    public List<EquipmentList> getEquipmentListNoSimulationByUserEmail(String userEmail){
        return equipListDao.getEquipmentListNoSimulationByUserEmail(userEmail);
    }

    public EquipmentList createEquipmentList(User user, boolean isSimulation, String name) {
        return equipListDao.createEquipmentList(user, isSimulation, name);
    }

    public EquipmentList getEquipmentListByListID(int id) {
        return equipListDao.getEquipmentListByListID(id);
    }

    public void deleteEquipmentList(EquipmentList list) {
        equipListDao.deleteEquipmentList(list);
    }

    /// EQUIPMENTLISTELEMENT ///
    public EquipmentListElement createEquipmentListElement(EquipmentList list, Equipment equipment, Date date, String frequency, Integer quantity) {
        return equipListElemDao.createEquipmentListElement(list, equipment, date, frequency, quantity);
    }

    public List<EquipmentListElement> getEquipmentListElementsByListId(int listId) {
        return equipListElemDao.getEquipmentListElementsByListId(listId);
    }

    public List<Equipment> getEquipmentsByListId(int listId) {
        return equipListElemDao.getEquipmentsByListId(listId);
    }

    public List<EquipmentListElement> getEquipmentListElementByListAndEquipment(int listId, Equipment equipment) {
        return equipListElemDao.getEquipmentListElementByListAndEquipment(listId, equipment);
    }

    // USER ///

    public User getUserByEmail(String email){
        return userDao.getUserByEmail(email);
    }

    /**
     * Renvoie null si l'utilisateur existe déjà sinon creer l'utilisateur et l'ajoute à la base de donnée
     * @param email
     * @param isProfessional
     * @param password
     * @param lastName
     * @param firstName
     * @param birthDate
     * @param photoURL
     * @return
     * @throws NoSuchAlgorithmException
     */
    public User createUser(String email, boolean isProfessional, String password,
                           String lastName, String firstName, Date birthDate,
                           String photoURL) throws NoSuchAlgorithmException {
        return userDao.createUser(email, isProfessional, Utility.sha256(password), lastName, firstName, birthDate, photoURL);
    }

    public void deleteUser(User user) {
        userDao.deleteUser(user);
    }

    public boolean isUserConnected(HttpServletRequest request){
        return getLoginTicketFromCookies(request) != null;
    }

    public User getCurrentUser(HttpServletRequest request) {
        LoginTicket loginTicket = getLoginTicketFromCookies(request);
        if (loginTicket == null) {
            return null;
        }
        return loginTicket.getUser();
    }

    public boolean updateUser(String email, boolean isProfessional, String passwordHashed,
                              String lastName, String firstName, Date birthDate,
                              String photoURL) {
        return userDao.updateUser(email, isProfessional, passwordHashed, lastName, firstName, birthDate, photoURL);
    }

    //LOGINTICKET

    public LoginTicket getLoginTicketById(int id){
        return loginTicketDAO.getLoginTicketById(id);
    }


    public LoginTicket getLoginTicketFromCookies(HttpServletRequest request) {
        Cookie cookieId = Utility.getCookie(request, "loginTicketId");
        Cookie cookieKey = Utility.getCookie(request, "loginTicketKey");
        if (cookieId == null || cookieKey == null) {
            System.out.println("One is null");
            return null;
        }
        LoginTicket loginTicket = getLoginTicketById(Integer.parseInt(cookieId.getValue()));
        if (loginTicket == null) {
            System.out.println("loginTicket null");
            return null;
        }
        if (!loginTicket.getKey().equals(cookieKey.getValue())) {
            System.out.println("not equals");
            return null;
        }

        return loginTicket;
    }

    public LoginTicket createLoginTicket(User user, long duration) throws NoSuchAlgorithmException {
        return loginTicketDAO.createLoginTicket(user, duration);
    }

    public void deleteLoginTicket(LoginTicket loginTicket) {
        loginTicketDAO.deleteLoginTicket(loginTicket);
    }

}
