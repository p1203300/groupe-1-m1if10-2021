package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "DeleteAccount", value = "/DeleteAccount")
public class DeleteAccount extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(DeleteAccount.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
        User user = wbc.getCurrentUser(request);
        if(user != null) {
            Utility.deleteCookie(request, response, "loginTicketId");
            Utility.deleteCookie(request, response, "loginTicketKey");
            wbc.deleteUser(user);
        }
        try {
            response.sendRedirect("../index.jsp");
        }catch (IOException e){
            logger.error("Exception happened !",e);
        }
    }
}
