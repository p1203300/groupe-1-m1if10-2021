package fr.univlyon1.m1if.m1if10.g1.servlet;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteList", value = "/DeleteList")
public class DeleteList extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(DeleteList.class);
    static final String REQUEST_MESSAGE = "RequestMessage";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestError = "RequestError";
        request.getServletContext().setAttribute(requestError, false);
        request.getServletContext().setAttribute(REQUEST_MESSAGE, "Votre liste d'équipements a bien été supprimée.");

        try {
            response.setContentType("text/html; charset=UTF-8");
            request.setCharacterEncoding("UTF-8");

            WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
            Integer listID = Integer.parseInt(request.getParameter("listNumber"));
            EquipmentList list = wbc.getEquipmentListByListID(listID);
            if(list.isSimulation()) {
                request.getServletContext().setAttribute(REQUEST_MESSAGE, "Votre simulation a bien été supprimée.");
            }
            wbc.deleteEquipmentList(list);
        }
        catch (Exception e) {
            request.getServletContext().setAttribute(requestError, true);
            request.getServletContext().setAttribute(REQUEST_MESSAGE, "Votre liste n'a pas pu être supprimée. Veuillez réessayer.");
        }
        try {
            response.sendRedirect("profil");
        }catch (IOException e){
            logger.error("Exception happened !",e);
        }
    }
}
