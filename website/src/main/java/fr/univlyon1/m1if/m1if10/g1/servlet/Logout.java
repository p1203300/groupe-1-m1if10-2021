package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "Logout", value = "/Logout")
public class Logout extends HttpServlet {

    final Logger logger = LoggerFactory.getLogger(Logout.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
        LoginTicket loginTicket = wbc.getLoginTicketFromCookies(request);
        try {

            if (loginTicket == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            Utility.deleteCookie(request, response, "loginTicketId");
            Utility.deleteCookie(request, response, "loginTicketKey");
            wbc.deleteLoginTicket(loginTicket);
            response.sendRedirect("../index.jsp");
        } catch (IOException e) {
            logger.error("Exception happened !", e);
        }
    }
}
