package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;


public class AuthenticationFilter extends HttpFilter {

    private boolean allowURL(String url) {
        return url.endsWith("/index.jsp") || url.endsWith("/seConnecter")
                || url.endsWith("/connexion")
                || url.endsWith("/deconnexion")
                || url.endsWith("/nouveauCompte")
                || url.endsWith("/creerCompte")
                || url.endsWith("/Controller")
                || url.endsWith("/Register")
                || url.endsWith("/Login")
                || url.endsWith("/Logout")
                || url.endsWith("/nouvelleListe")
                || url.endsWith("/SaveListe")
                || url.endsWith("/contact_us.jsp")
                || url.endsWith("/about_us.jsp")
                || url.endsWith("/help.jsp")
                || url.endsWith("/calcul.jsp")
                || url.endsWith(".css")
                || url.endsWith(".js")
                || url.toLowerCase().endsWith(".jpg")
                || url.toLowerCase().endsWith(".svg")
                || url.toLowerCase().endsWith(".png");
    }

    private boolean isURLExists(String servletPath) {
        String[] listURL = {"/carbon/connexion",
                "/carbon/seConnecter",
                "/carbon/deconnexion",
                "/carbon/nouveauCompte",
                "/carbon/creerCompte",
                "/carbon/profil",
                "/carbon/nouvelleListe",
                "/carbon/modifierListe",
                "/carbon/supprimerListe",
                "/carbon/enregistrerListe",
                "/carbon/supprimerCompte",
                "/carbon/editProfil",
                "/carbon/nouvelleSimulation",
                "/carbon/modifierSimulation",
                "/authentification.jsp",
                "/connexion.jsp",
                "/delete_account.jsp",
                "/contact_us.jsp",
                "/about_us.jsp",
                "/help.jsp",
                "/index.jsp",
                "/list.jsp",
                "/profile.jsp",
                "/registration.jsp",
                "/edit_profil.jsp",
                "/"
        };

        return (servletPath.toLowerCase().endsWith(".js") || servletPath.toLowerCase().endsWith(".css")
                || servletPath.toLowerCase().endsWith(".png")
                || servletPath.toLowerCase().endsWith(".svg")
                || servletPath.toLowerCase().endsWith(".jpg")
                || ArrayUtils.contains(listURL, servletPath));

    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String servletPath = request.getRequestURI().substring(request.getContextPath().length());
        if (!isURLExists(servletPath)) {
            request.setAttribute("msg", "404 NOT FOUND :" + request.getRequestURI());
            request.getRequestDispatcher("/error.jsp").forward(request, response);
            return;
        }
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
        LoginTicket loginTicket = wbc.getLoginTicketFromCookies(request);
        request.setAttribute("userConnected", loginTicket != null);
        if (servletPath.equals("/")) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
            chain.doFilter(request, response);
        } else {
            if (allowURL(request.getRequestURI())) {
                chain.doFilter(request, response);
            } else {
                if (loginTicket == null) {
                    response.sendRedirect(request.getContextPath() + "/index.jsp");
                    return;
                }
                if (loginTicket.isExpirated()) {
                    Utility.deleteCookie(request, response, "loginTicketId");
                    Utility.deleteCookie(request, response, "loginTicketKey");
                    wbc.deleteLoginTicket(loginTicket);
                    request.setAttribute("userConnected", false);
                    response.sendRedirect(request.getContextPath() + "/index.jsp");
                    return;
                }
                chain.doFilter(request, response);
            }
        }
    }
}
