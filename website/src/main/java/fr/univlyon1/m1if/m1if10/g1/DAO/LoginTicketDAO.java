package fr.univlyon1.m1if.m1if10.g1.DAO;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;

public class LoginTicketDAO {

    /**
     * Entity manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public LoginTicketDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     *  Creer un loginTicket et l'enregistre en base.
     * @param user
     * @param duration en millisecondes.
     * @return un LoginTicket.
     * @throws NoSuchAlgorithmException
     */
    public LoginTicket createLoginTicket(final User user, final long duration)
        throws NoSuchAlgorithmException {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime end = Utility.convertMillsToLocalDateTime(
                                    (new Date()).getTime() - duration);
        LoginTicket loginTicket = new LoginTicket(user, now, end);
        em.getTransaction().begin();
        em.persist(loginTicket);
        em.getTransaction().commit();
        return loginTicket;
    }

    /**
     * recupere un loginTicket en fonction de l'Id.
     * @param id
     * @return un LoginTicket.
     */
    public LoginTicket getLoginTicketById(final int id) {
        return em.find(LoginTicket.class, id);
    }

    /**
     * supprime un login ticket.
     * @param loginTicket
     */
    public void deleteLoginTicket(final LoginTicket loginTicket) {
        em.getTransaction().begin();
        em.remove(loginTicket);
        em.getTransaction().commit();
        em.clear();
    }

    /**
     * supprime tous les login tickets.
     * @param user
     */
    public void deleteAllLoginTicket(final User user) {
        em.getTransaction().begin();
        Query q = em.createQuery("DELETE FROM LoginTicket lt"
            + "WHERE lt.user.email = ?1");
        q.setParameter(1, user.getEmail());
        em.getTransaction().commit();
    }
}
