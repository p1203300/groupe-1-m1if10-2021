package fr.univlyon1.m1if.m1if10.g1.modele;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** Element d'une liste d'équipement.   */
@Entity
@Table(name = "list_contains_equipment")
public class EquipmentListElement {

    /** Identifiant.    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "list_contains_equipment_id")
    private int id;

    /** Liste d'équipement contenant l'élément. */
    @ManyToOne
    @JoinColumn(name = "equipments_list_id")
    private EquipmentList equipmentList;

    /** Équipement présent dans la liste d'équipement.  */
    @ManyToOne
    @JoinColumn(
            name = "category_title", referencedColumnName = "category_title")
    @JoinColumn(
            name = "equipment_name", referencedColumnName = "equipment_name")
    private Equipment equipment;

    /** Fréquence d'utilisation de l'équipement.    */
    @Column(name = "usage_frequency")
    private String usageFrequency;

    /** Date de mise en service.    */
    @Column(name = "commission_date")
    private Date commissionDate;

    /**
     * Nombre d'équipement de ce genre avec la même date de mise en service
     * dans la liste.
     */
    @Column(name = "quantity")
    private int quantity;

    /** Constructeur par défaut.    */
    public EquipmentListElement() {
    }

    /**
     * @param pEquipmentList    La liste ou ajouter l'équipement.
     * @param pEquipment    L'équipement à ajouter à la liste.
     * @param pDate La date de mise en service de(s) (l')équipement(s).
     * @param pFrequency La fréquence d'utilisation.
     * @param pQuantity Le nombre d'équipement.
     *
     * @brief Constructeur: Prend en paramètre la liste d'équipement cible,
     *  l'équipement à ajouter à la liste, la date de mise en service, la
     *  fréquence d'utilisation et le nombre d'équipement.
     */
    public EquipmentListElement(final EquipmentList pEquipmentList,
                                final Equipment pEquipment, final Date pDate,
                                final String pFrequency,
                                final Integer pQuantity) {
        this.equipmentList = pEquipmentList;
        this.equipment = pEquipment;
        this.commissionDate = pDate;
        this.usageFrequency = pFrequency;
        this.quantity = pQuantity;
    }

    /**
     * Renvoie l'identifiant en base de donnée.
     *
     * @return l'identifiant en base de donnée.
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie la liste contenant l'élément.
     *
     * @return  la liste contenant l'élément.
     */
    public EquipmentList getEquipmentList() {
        return equipmentList;
    }

    /**
     * Renvoie l'équipement.
     *
     * @return l'équipement.
     */
    public Equipment getEquipement() {
        return equipment;
    }

    /**
     * Renvoie la fréquence d'utilisation de l'équipement.
     *
     * @return  la fréquence d'utilisation de l'équipement.
     */
    public String getUsageFrequency() {
        return usageFrequency;
    }

    /**
     * Renvoie la date de mise en service de l'équipement.
     *
     * @return la date de mise en service de l'équipement.
     */
    public Date getCommissionDate() {
        return commissionDate;
    }

    /**
     * Renvoie le nombre d'équipement cible.
     *
     * @return le nombre d'équipement cible.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param e Nouveau type d'équipement.
     *
     * @brief   Changer le type d'équipement.
     */
    public void setEquipement(final Equipment e) {
        this.equipment = e;
    }

    /**
     * @param pUsageFrequency   Nouvelle fréquence d'utilisation
     *
     * @brief Changer la fréquence d'utilisation.
     */
    public void setUsageFrequency(final String pUsageFrequency) {
        this.usageFrequency = pUsageFrequency;
    }

    /**
     * @param pCommissionDate Nouvelle date de mise en service.
     *
     * @brief   Changer la date de mise en service.
     */
    public void setCommissionDate(final Date pCommissionDate) {
        this.commissionDate = pCommissionDate;
    }

    /**
     * @param pQuantity Nouvelle quantité.
     *
     * @brief Changer le nombre d'équipement de ce type ajoutée à la liste.
     */
    public void setQuantity(final int pQuantity) {
        this.quantity = pQuantity;
    }
}
