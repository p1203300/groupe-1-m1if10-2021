package fr.univlyon1.m1if.m1if10.g1.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Controller", value = "/carbon/*")
public class Controller extends HttpServlet {
    static final String USER_NO_CO = "userNotConnected";
    static final String LIST_JSP = "../list.jsp";
    static final String SIMULATION = "simulation";
    static final String EXCEPTION_TXT = "Exception happened !";
    final Logger logger = LoggerFactory.getLogger(Controller.class);


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String servletPath = request.getRequestURI().substring(request.getContextPath().length());
        try {
            if (servletPath.equals("/carbon/connexion")) {
                request.getRequestDispatcher("../authentification.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/seConnecter")) {
                request.getRequestDispatcher("../Login").forward(request, response);
            } else if (servletPath.equals("/carbon/deconnexion")) {
                request.getRequestDispatcher("../Logout").forward(request, response);
            } else if (servletPath.equals("/carbon/nouveauCompte")) {
                if (request.getParameter(USER_NO_CO) != null) {
                    request.getServletContext().setAttribute(USER_NO_CO, true);
                }
                request.getRequestDispatcher("../registration.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/creerCompte")) {
                request.getRequestDispatcher("../Register").forward(request, response);
            } else if (servletPath.equals("/carbon/profil")) {
                request.getRequestDispatcher("../profile.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/nouvelleListe")) {
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.equals("/carbon/enregistrerListe")) {
                request.getRequestDispatcher("../SaveList").forward(request, response);
            } else if (servletPath.contains("/carbon/modifierListe")) {
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.contains("/carbon/supprimerListe")) {
                request.getRequestDispatcher("../DeleteList").forward(request, response);
            } else if (servletPath.contains("/carbon/nouvelleSimulation")) {
                request.setAttribute(SIMULATION, true);
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.contains("/carbon/modifierSimulation")) {
                request.setAttribute(SIMULATION, true);
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.equals("/carbon/supprimerCompte")) {
                request.getRequestDispatcher("../DeleteAccount").forward(request, response);
            } else if (servletPath.equals("/carbon/editProfil")) {
                request.getRequestDispatcher("../EditProfil").forward(request, response);
            }
        } catch (ServletException | IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String servletPath = request.getRequestURI().substring(request.getContextPath().length());
        try {
            if (servletPath.equals("/carbon/connexion")) {
                request.getRequestDispatcher("../authentification.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/seConnecter")) {
                request.getRequestDispatcher("../Login").forward(request, response);
            } else if (servletPath.equals("/carbon/deconnexion")) {
                request.getRequestDispatcher("../Logout").forward(request, response);
            } else if (servletPath.equals("/carbon/nouveauCompte")) {
                if (request.getParameter(USER_NO_CO) != null) {
                    request.getServletContext().setAttribute(USER_NO_CO, true);
                }
                request.getRequestDispatcher("../registration.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/creerCompte")) {
                request.getRequestDispatcher("../Register").forward(request, response);
            } else if (servletPath.equals("/carbon/profil")) {
                request.getRequestDispatcher("../profile.jsp").forward(request, response);
            } else if (servletPath.equals("/carbon/nouvelleListe")) {
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.equals("/carbon/enregistrerListe")) {
                request.getRequestDispatcher("../SaveList").forward(request, response);
            } else if (servletPath.contains("/carbon/modifierListe")) {
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.contains("/carbon/supprimerListe")) {
                request.getRequestDispatcher("../DeleteList").forward(request, response);
            } else if (servletPath.contains("/carbon/nouvelleSimulation")) {
                request.setAttribute(SIMULATION, true);
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.contains("/carbon/modifierSimulation")) {
                request.setAttribute(SIMULATION, true);
                request.getRequestDispatcher(LIST_JSP).forward(request, response);
            } else if (servletPath.equals("/carbon/supprimerCompte")) {
                request.getRequestDispatcher("../DeleteAccount").forward(request, response);
            } else if (servletPath.equals("/carbon/editProfil")) {
                request.getRequestDispatcher("../edit_profil.jsp").forward(request, response);
            }
        } catch (ServletException | IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }
    }
}
