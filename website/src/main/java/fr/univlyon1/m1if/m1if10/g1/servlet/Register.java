package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "Register", value = "/Register")
public class Register extends HttpServlet {

    final Logger logger = LoggerFactory.getLogger(Register.class);
    static final String EXCEPTION_TXT = "Exception happened !";

    /**
     * Minimum length.
     */
    public static final int MAX_LENGTH_URL = 4096;
    /**
     * Maximum length.
     */
    public static final int MAX_LENGTH = 255;
    /**
     * Mois en jours.
     */
    public static final int MOIS = 30;
    /**
     * Jours en heure?
     */
    public static final int JOURS = 24;
    /**
     * Heure en minutes.
     */
    public static final int HEURES = 60;
    /**
     * Minute en secondes.
     */
    public static final int MINUTES = 60;
    /**
     * Seconde en millisecondes.
     */
    public static final int SECONDES = 1000;

    /**
     * Verifie si la taille es parametres sont correct.
     *
     * @param email
     * @param lastName
     * @param firstName
     * @param photoURL
     * @return vrai si les tailles sont corrects faux sinon.
     */
    boolean checkLength(final String email, final String lastName,
                        final String firstName, final String photoURL) {
        return ((email != null && email.length() > MAX_LENGTH)
                || (lastName != null && lastName.length() > MAX_LENGTH)
                || (firstName != null && firstName.length() > MAX_LENGTH)
                || (photoURL != null && photoURL.length() > MAX_LENGTH_URL));
    }

    /**
     * doPost.
     */
    @Override
    protected void doPost(final HttpServletRequest request,
                          final HttpServletResponse response)
            throws ServletException, IOException {
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext()
                .getAttribute("webSiteCarbon");
        String email = request.getParameter("email");
        boolean isProfessional = request.getParameter("professional") != null;
        String password = request.getParameter("password");
        String lastName = request.getParameter("last_name");
        String firstName = request.getParameter("first_name");
        String photoURL = request.getParameter("photo_url");
        Date birthDate = null;
        String birthDateStr = request.getParameter("birth_date");
        try {
            if (checkLength(email, lastName, firstName, photoURL)) {

                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            if (email == null || email.equals("")
                    || password == null
                    || password.equals("")) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            if (!Utility.isValidEmail(email)) {
                /*
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);*/
                request.setAttribute("invalid_email", true);
                try{
                    request.getRequestDispatcher("registration.jsp").include(request, response);
                } catch (ServletException | IOException e) {
                    logger.error(EXCEPTION_TXT, e);
                }
                return;
            }

            if (!Utility.isValidPassword(password)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        } catch (IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }
        if (birthDateStr != null && !birthDateStr.equals("")) {
            try {
                birthDate = (new SimpleDateFormat("yyyy-MM-dd"))
                        .parse(birthDateStr);
            } catch (ParseException e) {
                logger.error(EXCEPTION_TXT, e);
            }
        }
        try {
            if (birthDate != null && !Utility.isValidBirthDate(birthDate)) {
                request.setAttribute("invalid_birth_date", true);
                request.getRequestDispatcher("registration.jsp")
                        .include(request, response);
                return;
            }

            if (wbc.getUserByEmail(email) != null) {
                request.setAttribute("user_already_exist", true);
                request.getRequestDispatcher("registration.jsp")
                        .include(request, response);
                return;
            }
        } catch (ServletException | IOException e) {
            logger.error(EXCEPTION_TXT, e);
        }

        try {
            User user = wbc.createUser(email, isProfessional,
                    password, lastName, firstName,
                    birthDate, photoURL);
            LoginTicket loginTicket = wbc.createLoginTicket(user, SECONDES
                    * MINUTES * HEURES
                    * JOURS * MOIS);
            Cookie cookieId = new Cookie("loginTicketId", ""
                    + loginTicket.getId());
            cookieId.setHttpOnly(true);
            cookieId.setSecure(true);
            cookieId.setPath(request.getContextPath() + "/");
            Cookie cookieKey = new Cookie("loginTicketKey",
                    loginTicket.getKey());
            cookieKey.setHttpOnly(true);
            cookieKey.setSecure(true);
            cookieKey.setPath(request.getContextPath() + "/");

            response.addCookie(cookieId);
            response.addCookie(cookieKey);

            if (request.getParameter("userNotConnected") != null) {
                request.getServletContext()
                        .setAttribute("createdLoginTicket", loginTicket);
                request.getServletContext()
                        .setAttribute("userNotConnected", true);
            }

            response.sendRedirect("profil");
        } catch (NoSuchAlgorithmException | IOException e) {
            logger.error(EXCEPTION_TXT, e);
            response.sendRedirect("../index.jsp");
        }
    }
}
