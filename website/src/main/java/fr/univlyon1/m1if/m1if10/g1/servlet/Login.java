package fr.univlyon1.m1if.m1if10.g1.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if10.g1.metier.WebSiteCarbon;
import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name ="Login", value ="/Login")
public class Login extends HttpServlet {

     final Logger logger = LoggerFactory.getLogger(Login.class);
     static final String EXCEPTION_TXT = "Exception happened !";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        WebSiteCarbon wbc = (WebSiteCarbon) getServletContext().getAttribute("webSiteCarbon");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (email == null || email.equals("") || password == null || password.equals("")) {
            try{
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } catch (IOException e) {
                logger.error(EXCEPTION_TXT,e);
            }
            return;
        }

        if (!Utility.isValidEmail(email)) {
            /*
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);*/
            request.setAttribute("invalid_email", true);
            try{
                request.getRequestDispatcher("authentification.jsp").include(request, response);
            } catch (ServletException | IOException e) {
                logger.error(EXCEPTION_TXT, e);
            }
            return;
        }
        

        User user = wbc.getUserByEmail(email);

        if (user == null) {
            request.setAttribute("user_not_found", true);
            try{
                request.getRequestDispatcher("authentification.jsp").include(request, response);
            } catch (ServletException | IOException e) {
                logger.error(EXCEPTION_TXT, e);
            }
            return;
        }
        try {
            if (!user.getPasswordHashed().equals(Utility.sha256(password))) {
                request.setAttribute("invalid_password", true);
                request.getRequestDispatcher("authentification.jsp").include(request, response);
                return;
            }
        } catch (NoSuchAlgorithmException | ServletException | IOException e1) {
            logger.error(EXCEPTION_TXT,e1);
        }

        try {
            LoginTicket loginTicket = wbc.createLoginTicket(user, 1000 *60*60*24*30);
            // Gestion des cookies
            Cookie cookieId = new Cookie("loginTicketId", "" + loginTicket.getId());
            cookieId.setHttpOnly(true);
            cookieId.setSecure(true);
            cookieId.setPath(request.getContextPath() + "/");
            Cookie cookieKey = new Cookie("loginTicketKey", loginTicket.getKey());
            cookieKey.setHttpOnly(true);
            cookieKey.setSecure(true);
            cookieKey.setPath(request.getContextPath() + "/");
            response.addCookie(cookieId);
            response.addCookie(cookieKey);

            if(request.getParameter("userNotConnected") != null) {
                request.getServletContext().setAttribute("createdLoginTicket", loginTicket);
                request.getServletContext().setAttribute("userNotConnected", true);
            }

            response.sendRedirect("profil");
        } catch (NoSuchAlgorithmException | IOException e) {
            logger.error(EXCEPTION_TXT,e);
            response.sendRedirect("../index.jsp");
        }
    }
    
}
