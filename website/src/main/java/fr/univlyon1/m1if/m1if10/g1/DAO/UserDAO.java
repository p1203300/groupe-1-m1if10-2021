package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.User;

import java.util.Date;

import javax.persistence.EntityManager;

public class UserDAO {
    /**
     * ENtiry manager.
     */
    private final EntityManager em;

    /**
     * Constructeur.
     * @param entityManager
     */
    public UserDAO(final EntityManager entityManager) {
        this.em = entityManager;
    }

    /**
     * recupere un user en fonction du mail.
     * @param email
     * @return un User.
     */
    public User getUserByEmail(final String email) {
        return em.find(User.class, email);
    }


    /**
     * Renvoie null si l'utilisateur existe déjà sinon creer l'utilisateur
     * et l'ajoute à la base de donnée.
     * @param email
     * @param isProfessional
     * @param passwordHashed
     * @param lastName
     * @param firstName
     * @param birthDate
     * @param photoURL
     * @return un User.
     */
    public User createUser(final String email, final boolean isProfessional,
                           final String passwordHashed, final String lastName,
                           final String firstName, final Date birthDate,
                           final String photoURL) {
        if (getUserByEmail(email) != null) {
            return null;
        }

        User user = new User(isProfessional, email, passwordHashed);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setBirthDate(birthDate);
        user.setPhotoURL(photoURL);

        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();

        return user;
    }

    /**
     * suprrime un user dans la bd.
     * @param user
     */
    public void deleteUser(final User user) {
        em.getTransaction().begin();
        em.remove(user);
        em.getTransaction().commit();
        em.clear();
    }

    /**
     * Renvoie null si l'utilisateur existe déjà sinon creer l'utilisateur et l'ajoute à la base de donnée
     * @param email
     * @param isProfessional
     * @param passwordHashed
     * @param lastName
     * @param firstName
     * @param birthDate
     * @param photoURL
     * @return
     */
    public boolean updateUser(String email, boolean isProfessional, String passwordHashed,
                           String lastName, String firstName, Date birthDate,
                           String photoURL) {
        User user = getUserByEmail(email);
        if (user == null) {
            return false;
        }

        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setPasswordHashed(passwordHashed);
        user.setBirthDate(birthDate);
        user.setPhotoURL(photoURL);
        user.setProfessional(isProfessional);

        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();

        return true;
    }
}