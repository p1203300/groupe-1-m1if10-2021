package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.User;
import org.junit.jupiter.api.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserDAOTest {
    UserDAO uDAO;
    EntityManager em;

    void resetDataBase() throws IOException {
        Path fileName = Path.of("src/main/resources/websitecarbon.sql");
        String actual = Files.readString(fileName);
        em.getTransaction().begin();
        em.createNativeQuery(actual).executeUpdate();
        em.getTransaction().commit();
        em.clear();
    }

    @BeforeAll
    void setUp() throws IOException {
        em = Persistence.createEntityManagerFactory("test_websitecarbon").createEntityManager();
        uDAO = new UserDAO(em);
        resetDataBase();
    }

    @AfterAll
    void endUp() {
        em.close();
    }

    @Test
    void createUser() {
        User u1;
        User u2;
        User u3;

        u1 = uDAO.createUser("Guillaume@ChefDuProjet.com", false, "passwordNotHashed", "Guy", "Chef", new Date(2012, 12, 20), "xxx.x");
        u2 = uDAO.getUserByEmail("Guillaume@ChefDuProjet.com");
        assertThat(u1, is(u2));

        u3 = uDAO.getUserByEmail("MauvaisGuillaume@ChefDuProjet.com");
        assertThat(u1, not(is(u3)));

        uDAO.deleteUser(uDAO.getUserByEmail("Guillaume@ChefDuProjet.com"));
    }

    @Test
    void createUserTwice() {
        User u1;
        User u2;

        u1 = uDAO.createUser("Guillaume@ChefDuProjet.com", false, "passwordNotHashed", "Guy", "Chef", new Date(2012, 12, 20), "xxx.x");
        u2 = uDAO.createUser("Guillaume@ChefDuProjet.com", false, "passwordNotHashed", "Guy", "Chef", new Date(2012, 12, 20), "xxx.x");
        assertThat(u2, not(is(u1)));
        assertThat(u2, is(nullValue()));

        uDAO.deleteUser(uDAO.getUserByEmail("Guillaume@ChefDuProjet.com"));
    }

    @Test
    void deleteUser() {
        User u1;
        User u2;

        u1 = uDAO.createUser("guigui@ChefDuProjet.com", false, "passwordNotHashed", "Guy", "Chef", new Date(2012, 12, 20), "xxx.x");
        uDAO.deleteUser(uDAO.getUserByEmail("guigui@ChefDuProjet.com"));
        u2 = uDAO.getUserByEmail("guigui@ChefDuProjet.com");
        assertThat(u1, not(is(u2)));
    }
}