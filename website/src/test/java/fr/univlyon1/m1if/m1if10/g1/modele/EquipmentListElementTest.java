package fr.univlyon1.m1if.m1if10.g1.modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class EquipmentListElementTest {
    private EquipmentListElement ele;
    private EquipmentList el;
    private Equipment e;

    @BeforeEach
    public void setUp() {
        el = new EquipmentList();
        e = new Equipment();
        this.ele = new EquipmentListElement(el, e, null, null, 42);
    }

    @Test
    void getEquipmentsList() {
        assertThat(this.ele.getEquipmentList(), sameInstance(this.el));
    }

    @Test
    void getAndSetEquipement() {
        Equipment e1 = new Equipment();
        assertThat(this.ele.getEquipement(), sameInstance(this.e));
        assertThat(this.ele.getEquipement(), not(sameInstance(e1)));

        this.ele.setEquipement(e1);
        assertThat(this.ele.getEquipement(), sameInstance(e1));
        assertThat(this.ele.getEquipement(), not(sameInstance(this.e)));
    }

    @Test
    void getUsageFrequency() {
        this.ele.setUsageFrequency("Je suis un USAGE FREQUENCY lol");
        assertThat(this.ele.getUsageFrequency(), is("Je suis un USAGE FREQUENCY lol"));
        this.ele.setUsageFrequency("TRES TRES TRES souvent");
        assertThat(this.ele.getUsageFrequency(), is("TRES TRES TRES souvent"));
    }

    @Test
    void getAndSetCommissionDate() {
        this.ele.setCommissionDate(new Date(2021, 05, 02));
        assertThat(this.ele.getCommissionDate().getTime(), is(new Date(2021, 05, 02).getTime()));
        this.ele.setCommissionDate(new Date(2011, 04, 05));
        assertThat(this.ele.getCommissionDate().getTime(), is(new Date(2011, 04, 05).getTime()));
    }

    @Test
    void getQuantity() {
        assertThat(ele.getQuantity(), is(42));
    }
}