package fr.univlyon1.m1if.m1if10.g1.modele;

import fr.univlyon1.m1if.m1if10.g1.utility.Utility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class LoginTicketTest {
    private LoginTicket lt;
    private User u;

    @BeforeEach
    public void setUp() throws NoSuchAlgorithmException {
        User u = new User();
        this.lt = new LoginTicket(u, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    void getKey() throws NoSuchAlgorithmException {
        assertThat(lt.getKey(), is(Utility.sha256(lt.getUser().getEmail() + lt.getUser().getPasswordHashed() + lt.getBeginDate().toString() + lt.getEndDate().toString())));
    }

    @Test
    void getAndSetBeginDate() {
        this.lt.setBeginDate(LocalDateTime.now());
        assertThat(this.lt.getBeginDate().getMinute(), is(LocalDateTime.now().getMinute()));

        this.lt.setBeginDate(LocalDateTime.now());
        assertThat(this.lt.getBeginDate().getMinute(), is(LocalDateTime.now().getMinute()));
    }

    @Test
    void getAndSetEndDate() {
        this.lt.setEndDate(LocalDateTime.now());
        assertThat(this.lt.getEndDate().getMinute(), is(LocalDateTime.now().getMinute()));

        this.lt.setEndDate(LocalDateTime.now());
        assertThat(this.lt.getEndDate().getMinute(), is(LocalDateTime.now().getMinute()));
    }

    @Test
    void getAndSetUser() {
        User u1 = new User();

        this.lt.setUser(this.u);
        assertThat(this.lt.getUser(), sameInstance(this.u));
        assertThat(this.lt.getUser(), not(sameInstance(u1)));

        this.lt.setUser(u1);
        assertThat(this.lt.getUser(), sameInstance(u1));
        assertThat(this.lt.getUser(), not(sameInstance(this.u)));
    }

}