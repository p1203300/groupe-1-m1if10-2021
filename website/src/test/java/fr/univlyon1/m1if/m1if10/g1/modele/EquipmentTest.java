package fr.univlyon1.m1if.m1if10.g1.modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class EquipmentTest {
    private Equipment e;
    private Category c;

    @BeforeEach
    public void setUp() {
        this.e = new Equipment();
        Category c = new Category();
        c.setTitle("Je suis ton titre");
        c.setDescription("Je suis ta description");
    }

    @Test
    void getAndSetCategory() {
        Category c1 = new Category();
        c1.setTitle("Je suis la mauvaise catégorie");
        c1.setDescription("Je suis la mauvaise description");

        this.e.setCategory(this.c);

        assertThat(this.e.getCategory(), sameInstance(this.c));
        assertThat(this.e.getCategory(), not(sameInstance(c1)));
        this.e.setCategory(c1);
        assertThat(this.e.getCategory(), not(sameInstance(this.c)));
        assertThat(this.e.getCategory(), sameInstance(c1));
        ;
    }

    @Test
    void getAndSetName() {
        this.e.setName("JE SUIS UN EQUIPEMENT TRIDIMENSIONNEL");
        assertThat(this.e.getName(), is("JE SUIS UN EQUIPEMENT TRIDIMENSIONNEL"));
        this.e.setName("1+1=2");
        assertThat(this.e.getName(), is("1+1=2"));
    }

    @Test
    void getAndSetAverageLifeDuration() {
        this.e.setAverageLifeDuration(3.0f);
        assertThat(this.e.getAverageLifeDuration(), is(3.0f));
        this.e.setAverageLifeDuration(23.2f);
        assertThat(this.e.getAverageLifeDuration(), is(23.2f));
    }

    @Test
    void getAndSetConstructionFootPrint() {
        this.e.setConstructionFootPrint(3.0f);
        assertThat(this.e.getConstructionFootPrint(), is(3.0f));
        this.e.setConstructionFootPrint(23.2f);
        assertThat(this.e.getConstructionFootPrint(), is(23.2f));
    }

    @Test
    void getAndSetUsageFootPrint() {
        this.e.setUsageFootPrint(3.0f);
        assertThat(this.e.getUsageFootPrint(), is(3.0f));
        this.e.setUsageFootPrint(23.2f);
        assertThat(this.e.getUsageFootPrint(), is(23.2f));
    }
}