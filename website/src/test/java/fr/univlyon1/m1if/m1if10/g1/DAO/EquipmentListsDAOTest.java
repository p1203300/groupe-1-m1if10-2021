package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.EquipmentList;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import org.junit.jupiter.api.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EquipmentListsDAOTest {
    EntityManager em;
    EquipmentListsDAO elDAO;
    UserDAO uDAO;

    void resetDataBase() throws IOException {
        Path fileName = Path.of("src/main/resources/websitecarbon.sql");
        String actual = Files.readString(fileName);
        em.getTransaction().begin();
        em.createNativeQuery(actual).executeUpdate();
        em.getTransaction().commit();
        em.clear();
    }


    @BeforeAll
    void setUp() throws Exception {
        em = Persistence.createEntityManagerFactory("test_websitecarbon").createEntityManager();
        elDAO = new EquipmentListsDAO(em);
        uDAO = new UserDAO(em);
        resetDataBase();
    }

    @AfterAll
    void endUp() {
        em.close();
    }


    @Test
    void getEquipmentListByUserEmail() {
        List<EquipmentList> tempEl;
        EquipmentList el1, el2;
        User u;

        u = uDAO.createUser("JeSuisUnTest@gmail.com", false, "123456789", "Guillaume", "Testeur", null, "");
        el1 = elDAO.createEquipmentList(u, false, "JeSuisUnTest1");
        el2 = elDAO.createEquipmentList(u, false, "JeSuisUnTest2");
        tempEl = elDAO.getEquipmentListByUserEmail("JeSuisUnTest@gmail.com");

        assertThat(tempEl, hasItem(el1));
        assertThat(tempEl, hasItem(el2));

        uDAO.deleteUser(u);
    }

    @Test
    void getEquipmentListNoSimulationByUserEmail() {
        List<EquipmentList> tempEl;
        EquipmentList el1, el2;
        User u;

        u = uDAO.createUser("JeSuisUnTest@gmail.com", false, "123456789", "Guillaume", "Testeur", null, "");
        el1 = elDAO.createEquipmentList(u, true, "JeSuisUnTest1");
        el2 = elDAO.createEquipmentList(u, false, "JeSuisUnTest2");
        tempEl = elDAO.getEquipmentListNoSimulationByUserEmail("JeSuisUnTest@gmail.com");

        assertThat(tempEl, not(hasItem(el1)));
        assertThat(tempEl, hasItem(el2));

        uDAO.deleteUser(u);
    }
}