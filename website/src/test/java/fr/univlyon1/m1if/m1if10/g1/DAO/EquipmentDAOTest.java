package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.Equipment;
import org.junit.jupiter.api.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EquipmentDAOTest {
    EntityManager em;
    EquipmentDAO eDAO;

    @BeforeAll
    void setUp() throws Exception {
        em = Persistence.createEntityManagerFactory("test_websitecarbon").createEntityManager();
        eDAO = new EquipmentDAO(em);
        Equipment e = new Equipment();
    }


    @AfterAll
    void endUp() {
        em.close();
    }


    @Test
    void getEquipmentByNameAndCategoryTitle() {
        Equipment e;

        e = eDAO.getEquipmentByNameAndCategoryTitle("Laptop", "Laptop bureautique");

        assertThat(e.getCategory().getTitle(), is("Laptop"));
        assertThat(e.getName(), is("Laptop bureautique"));
        assertThat(e.getAverageLifeDuration(), is(4.0F));

        e = eDAO.getEquipmentByNameAndCategoryTitle("INEXISTANT LOL", "IMPOSSIBLE LOL");

        assertThat(e, is(nullValue()));
    }


    @Test
    void getEquipmentsByCategory() {
        List<Equipment> eL;
        Equipment e;

        eL = eDAO.getEquipmentsByCategory("Laptop");

        e = eDAO.getEquipmentByNameAndCategoryTitle("Laptop", "Laptop bureautique");
        assertThat(eL, hasItem(e));
        e = eDAO.getEquipmentByNameAndCategoryTitle("Laptop", "Laptop haute performance");
        assertThat(eL, hasItem(e));
        e = eDAO.getEquipmentByNameAndCategoryTitle("Laptop", "Laptop MacBook");
        assertThat(eL, hasItem(e));
    }
}
