package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.Category;
import org.junit.jupiter.api.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CategoryDAOTest {
    EntityManager em;
    CategoryDAO cDAO;

    ArrayList<String> getAllTitle() {
        ArrayList<String> tempList = new ArrayList<String>();
        List<Category> lC = null;
        lC = cDAO.getCategoryList();

        for (int i = 0; i < lC.size(); i++) {
            tempList.add(lC.get(i).getTitle());
        }

        return tempList;
    }

    @BeforeAll
    void setUp() throws Exception {
        em = Persistence.createEntityManagerFactory("test_websitecarbon").createEntityManager();
        cDAO = new CategoryDAO(em);
    }

    @AfterAll
    void endUp() {
        em.close();
    }

    @Test
    void getCategoryList() {
        ArrayList<String> listTitleCat = getAllTitle();

        assertThat(listTitleCat, hasItem("Télévision"));
        assertThat(listTitleCat, hasItem("Laptop"));
        assertThat(listTitleCat, hasItem("Liseuse"));
        assertThat(listTitleCat, hasItem("Modem"));
        assertThat(listTitleCat.size(), is(22));
        assertThat(listTitleCat.size(), not(is(2)));
    }

    @Test
    void getCategoryByNom() {
        assertThat(cDAO.getCategoryByNom("Télévision").getTitle(), is("Télévision"));
        assertThat(cDAO.getCategoryByNom("Télévision").getDescription(), is("Télévision."));
    }
}