package fr.univlyon1.m1if.m1if10.g1.modele;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class UserTest {
    private User u;

    @BeforeEach
    public void setUp() {
        this.u = new User();
    }

    @Test
    void getEmail() {
        User u1 = new User(false, "guillaume@gmail.com", "password");
        assertThat(u1.getEmail(), is("guillaume@gmail.com"));
    }

    @Test
    void isProfessional() {
        User u1 = new User(false, "guillaume@gmail.com", "password");
        assertThat(u1.isProfessional(), is(false));
    }

    @Test
    void getAndSetPassword() throws NoSuchAlgorithmException {
        this.u.setPasswordHashed("JeSuisUnMotDePasse");
        assertThat(this.u.getPasswordHashed(), is("JeSuisUnMotDePasse"));

        this.u.setPasswordHashed("MotDePasseIncassable");
        assertThat(this.u.getPasswordHashed(), is("MotDePasseIncassable"));
    }

    @Test
    void getAndSetLastName() {
        this.u.setLastName("Guillaume");
        assertThat(this.u.getLastName(), is("Guillaume"));

        this.u.setLastName("Patrick");
        assertThat(this.u.getLastName(), is("Patrick"));
    }

    @Test
    void getAndSetFirstName() {
        this.u.setFirstName("Dark-Vador");
        assertThat(this.u.getFirstName(), is("Dark-Vador"));

        this.u.setFirstName("King Kong");
        assertThat(this.u.getFirstName(), is("King Kong"));
    }

    @Test
    void getAndSetBirthDate() {
        this.u.setBirthDate(new Date(1989, 1, 5));
        assertThat(this.u.getBirthDate(), is(new Date(1989, 1, 5)));

        this.u.setBirthDate(new Date(2000, 5, 7));
        assertThat(this.u.getBirthDate(), is(new Date(2000, 5, 7)));
    }

    @Test
    void getAndSetPhotoURL() {
        this.u.setPhotoURL("http://google.fr");
        assertThat(this.u.getPhotoURL(), is("http://google.fr"));

        this.u.setPhotoURL("http://firefox.fr");
        assertThat(this.u.getPhotoURL(), is("http://firefox.fr"));
    }
}