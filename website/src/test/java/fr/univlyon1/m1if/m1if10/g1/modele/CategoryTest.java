package fr.univlyon1.m1if.m1if10.g1.modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class CategoryTest {
    private Category c;

    @BeforeEach
    public void setUp() {
        this.c = new Category();
    }

    @Test
    void getAndSetDescription() {
        this.c.setDescription("Je suis une description");
        assertThat(this.c.getDescription(), is("Je suis une description"));
        this.c.setDescription("Je suis ton père");
        assertThat(this.c.getDescription(), is("Je suis ton père"));
    }

    @Test
    void getAndSetTitle() {
        this.c.setTitle("C'est moi le chef des tests");
        assertThat(this.c.getTitle(), is("C'est moi le chef des tests"));
        this.c.setTitle("Personne ne peut remettre en question mon autorité");
        assertThat(this.c.getTitle(), is("Personne ne peut remettre en question mon autorité"));
    }
}