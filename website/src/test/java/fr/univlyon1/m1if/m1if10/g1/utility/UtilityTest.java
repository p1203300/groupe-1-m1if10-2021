package fr.univlyon1.m1if.m1if10.g1.utility;


import org.junit.jupiter.api.*;


import java.time.LocalDate;

import java.util.Date;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UtilityTest {

    @Test
    void isValidPassword() {
        String password = "JeSuisUnLongMotDePasse";
        assertThat(Utility.isValidPassword(password), is(true));
        password = "Court";
        assertThat(Utility.isValidPassword(password), is(false));
    }

    @Test
    void isValidEmail() {
        String email = "JeSuisUn@gmail.com";
        assertThat(Utility.isValidEmail(email), is(true));
        email = "Jesuis.com";
        assertThat(Utility.isValidEmail(email), is(false));
    }

    @Test
    void isValidBirthDate() {
        Date birthdate = new Date(2000, 7, 5);
        //assertThat(Utility.isValidBirthDate(birthdate),is(true));
        birthdate = new Date(2121, 11, 12);
        assertThat(Utility.isValidBirthDate(birthdate), is(false));
        birthdate = new Date(1821, 11, 12);
        assertThat(Utility.isValidBirthDate(birthdate), is(false));
    }

    @Test
    void calculateAge() {
        LocalDate birthdate = LocalDate.of(2000, 7, 5);
        assertThat(Utility.calculateAge(birthdate, LocalDate.now()), is(21));
    }
}