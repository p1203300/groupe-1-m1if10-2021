package fr.univlyon1.m1if.m1if10.g1.modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class EquipmentListTest {
    private EquipmentList el1;
    private EquipmentList el2;
    private User u;

    @BeforeEach
    public void setUp() {
        u = new User();
        this.el1 = new EquipmentList();
        this.el2 = new EquipmentList(u, false, "NomPasImportant");
    }

    @Test
    void getAndSetName() {
        this.el1.setName("Pourquoi j'ai pris le taff de faire des tests");
        assertThat(this.el1.getName(), is("Pourquoi j'ai pris le taff de faire des tests"));

        this.el1.setName("Pourquoi 11 UE ?");
        assertThat(this.el1.getName(), is("Pourquoi 11 UE ?"));

        assertThat(this.el2.getName(), is("NomPasImportant"));
    }

    @Test
    void getAndSetLastModificationDate() {
        this.el1.setLastModificationDate(LocalDateTime.now());
        assertThat(this.el1.getLastModificationDate().getMinute(), is(LocalDateTime.now().getMinute()));

        this.el1.setLastModificationDate(LocalDateTime.now());
        assertThat(this.el1.getLastModificationDate().getMinute(), is(LocalDateTime.now().getMinute()));
    }

    @Test
    void getUser() {
        u.setFirstName("Guillaume");
        assertThat(el2.getUser().getFirstName(), is("Guillaume"));
    }

}