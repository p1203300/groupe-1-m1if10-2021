package fr.univlyon1.m1if.m1if10.g1.DAO;

import fr.univlyon1.m1if.m1if10.g1.modele.LoginTicket;
import fr.univlyon1.m1if.m1if10.g1.modele.User;
import org.junit.jupiter.api.*;

import static org.hamcrest.MatcherAssert.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;

import static org.hamcrest.CoreMatchers.*;

import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LoginTicketDAOTest {
    EntityManager em;
    LoginTicketDAO lDAO;
    UserDAO uDAO;

    void resetDataBase() throws IOException {
        Path fileName = Path.of("src/main/resources/websitecarbon.sql");
        String actual = Files.readString(fileName);
        em.getTransaction().begin();
        em.createNativeQuery(actual).executeUpdate();
        em.getTransaction().commit();
        em.clear();
    }

    @BeforeAll
    void setUp() throws IOException {
        em = Persistence.createEntityManagerFactory("test_websitecarbon").createEntityManager();
        lDAO = new LoginTicketDAO(em);
        uDAO = new UserDAO(em);
        resetDataBase();
    }

    @AfterAll
    void endUp() {
        em.close();
    }

    @Test
    void getLoginTicketById() throws NoSuchAlgorithmException {
        User u = uDAO.createUser("JeSuisUnTest@gmail.com", false, "123456789", "Guillaume", "Testeur", null, "");
        LoginTicket lt = lDAO.createLoginTicket(u, 5);

        TypedQuery<Integer> q = em.createQuery("SELECT lt.id " +
                        "FROM LoginTicket lt WHERE lt.user.email = 'JeSuisUnTest@gmail.com'"
                , Integer.class);
        List<Integer> results = q.getResultList();
        int ticket_id = results.get(0);

        assertThat(lDAO.getLoginTicketById(ticket_id), is(lt));

        uDAO.deleteUser(u);
    }
}